'use strict';

var src = { public: 'public/' };

var gulp = require('gulp'),
    webserver = require('gulp-webserver'),
    minifyCSS = require('gulp-minify-css'),
    concat = require('gulp-concat'),
    wiredep = require('wiredep').stream,
    autoprefixer = require('gulp-autoprefixer');

gulp.task('css', function () {
  return gulp.src(src.public + 'css/**/*.css')
    .pipe(minifyCSS())
    .pipe(concat('main.min.css'))
    .pipe(autoprefixer())
    .pipe(gulp.dest(src.public + 'dist/css'));
});
 
gulp.task('js', function() {
  return gulp.src(src.public + 'app/**/**/*.js')
    .pipe(concat('main.js'))
    .pipe(gulp.dest(src.public + 'dist/js/'));
});

gulp.task('default', function () {
  gulp.watch(src.public + 'css/**/*.css', ['css']);
  gulp.watch(src.public + 'app/**/**/*.js', ['js']);
  gulp.watch('bower.json', ['bower']);
});

gulp.task('bower', function () {
  gulp.src(src.public + 'index.html')
    .pipe(wiredep({
      directory: src.public + 'bower_components'
    }))
    .pipe(gulp.dest(src.public));
});
