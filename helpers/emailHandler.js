/*
FileName : emailHandler.js
Date : 21st Sep 2017
Description : This file consist of function to send emails
*/

/* DEPENDENCIES */
var utf8 = require('utf8');
var config = require('./../config/config.constants');
var fs = require('fs');
var postmark = require("postmark");

// Send an email:
var client = new postmark.Client(config.postmarkClientKey);

/* Sends mail while creating user account or admin account*/
exports.sendDriverRegistrationMail = function (userMailAddress, userPassword, userName, verificationLink) {
  client.sendEmail({
    "From": config.postmarkEmailAddress,
    "To": userMailAddress,
    "Subject": "Welcome To RaceAgent",
    "HtmlBody": 'Your RaceAgent Sponsorship Engine account has been created! We would like to welcome you to the team. You now have access to thousands of decision makers all at a click of a button. Search locally or nationally for those funds to take you or your team to that next step. If you have any questions don’t hesitate to call our office or simply shoot us an email at info@teamraceagent.com. We look forward to seeing your success on the industry’s leading sponsorship platform! <br>Here are your keys:<br><br>User Name : ' + userName + '<br>Email : ' + userMailAddress + '<br>Password : ' + userPassword + '<br>Please visit the following link to verify your email address<br>' + verificationLink
  }, function (error, result) {
    if (error) {
      console.error("Unable to send via postmark: " + error.message);
      return;
    }
    console.info("Sent to postmark for delivery")
  });
};

/* Sends mail when a user contacts company */
exports.sendUserContactCompanyMessages = function (toMail, mailHash, subject, message, attachment) {
  client.sendEmail({
    "From": config.postmarkEmailAddress,
    "ReplyTo": "c3f434f3a37913e431548964b44686aa+" + mailHash + "@inbound.postmarkapp.com",
    "To": toMail,
    "Subject": subject,
    "HtmlBody": message,
    "Attachments": attachment
  }, function (error, result) {
    if (error) {
      console.error("Unable to send via postmark: " + error.message);
      return;
    }
    console.info("Sent to postmark for delivery")
  });
};

/* Send forgot password mail */
exports.sendForgotPasswordMail = function (email, resetPasswordLink) {
  client.sendEmail({
    "From": config.postmarkEmailAddress,
    "To": email,
    "Subject": "RaceAgent Password Reset",
    "HtmlBody": 'Please visit the following link to reset your password.<br>' + resetPasswordLink
  }, function (error, result) {
    if (error) {
      console.error("Unable to send via postmark: " + error.message);
      return;
    }
    console.info("Sent to postmark for delivery")
  });
};
