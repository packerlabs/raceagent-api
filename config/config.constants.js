/*
FileName : config.constants.js
Date : 26th June 2017
Description : This file consist of list of contants to use in the APIs
*/

module.exports = {
    'secret': 'ad123123A@!@#ADAsdad@#!@',
    'database' : "mongodb://raceagent:raceagentPassword@127.0.0.1:27017/raceagent-api",
    'serverURL' : "https://www.raceagentengine.com",
    'postmarkEmailAddress' : "pitcrew@raceagentengine.com",
    'postmarkClientKey' : '88b54c23-9599-436f-9ed0-3b3852bb7a53',
    'adminEmail' : 'admin@raceagent.com',
    'adminPassword' : '$2a$06$4MSfImAVF91.jNhEj0VkT.Ug6MbxcidZipILQ36CMwPKaK8I1g/pK',
    'stripeTestKey' : 'sk_live_a4m27XxDyIBMtZGZfS6RBIMW'
};
