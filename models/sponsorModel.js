  /*
FileName : sponsorModel.js
Date : 2nd July 2017
Description : This file consist of driver users data model
*/

var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var ObjectId = Schema.ObjectId;


/* set up a mongoose model */

var sponsorSchema = new Schema({
  sponsorName: {
    type: String,
    required: true
  },
  companyName: {
    type: String,
    required: true
  },
  sponsorNumber: {
    type: String,
    required: true
  },
  sponsorAmmount: {
    type: String
  },
  partnerCompanyId: [{type: Schema.Types.ObjectId, ref: 'Company' }],
  createdOn: {
    type: Date
  }
});


var Sponsor = mongoose.model('Sponsor', sponsorSchema);
module.exports = Sponsor;
