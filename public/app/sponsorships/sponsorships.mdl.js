(function () {
    'use strict';

    angular
        .module('main.sponsorships', [])
        .config(config);

    config.$inject = ['$stateProvider', '$urlRouterProvider', '$httpProvider'];
    function config($stateProvider, $urlRouterProvider, $httpProvider) {
        
        $stateProvider
            .state('main.sponsorships', {
                url: 'sponsorships',
                templateUrl: '../views/sponsorships/sponsorships.html',
                controller: 'SponsorshipsCtrl as vm',
                data: {
                    is_granted: ['ROLE_DRIVER']
                }
            });
        
    }

})();
 