(function () {
    'use strict';

    angular
        .module('main')
        .controller('SponsorshipsCtrl', SponsorshipsCtrl);

    function SponsorshipsCtrl($log, SponsorsService, Notification) {
        var vm = this;
        vm.sponsors = undefined;
        vm.params = {
            page: 1,
            limit: 10
        };
        vm.getSponsors = getSponsors;
        vm.getSponsors();
        function getSponsors() {

            function success(response) {
                vm.sponsors = response.data.data.sponsorData;
                vm.totalRecords = response.data.data.totalRecords;
            }
 
            function failed(response) {
                $log.error(response.data.message);
            }

            SponsorsService
                .getSponsors(vm.params)
                .then(success, failed);
        }

	}

})();
 