(function () {
    'use strict';

    angular
        .module('main.settings', [])
        .config(config);

    config.$inject = ['$stateProvider', '$urlRouterProvider', '$httpProvider'];
    function config($stateProvider, $urlRouterProvider, $httpProvider) {
        
        $stateProvider
            .state('main.settings', {
                url: 'settings',
                views: {
                    '@main': {
                        templateUrl: '../views/settings/settings.html',
                        controller: 'DriverAccountCtrl as vm'
                    }
                }
            });
          }
})(); 
 