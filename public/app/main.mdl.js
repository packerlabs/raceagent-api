(function () {
    'use strict';

    angular
        .module('main', [
            'ui.router',
            'ui.bootstrap',
            'ngMask',
            'ngCookies',
            'ngAnimate',
            'ngDialog',
            'cr.acl',
            'flow',
            'checklist-model',
            'ui-notification',
            'uiSwitch',
            'nvd3', 
            'ngFlash',
            'textAngular',
            'ngSanitize',
            'ngImgCrop',
            'localytics.directives',
            'jQueryScrollbar',

            'main.favorites',
            'main.companies',
            'main.loading',
            'main.driver',
            'main.search',
            'main.settings',
            'main.membership',
            'main.mailbox',
            'main.welcome',
            'main.partners',
            'main.sponsorships',

            'main.auth',

            'admin',
            'config'
        ])
        .config(config)
        .run(run);

    config.$inject = ['$stateProvider', '$urlRouterProvider', 'NotificationProvider'];
    function config($stateProvider, $urlRouterProvider, NotificationProvider) {

        $urlRouterProvider.otherwise(function ($injector) {
            var $state = $injector.get('$state');
            var $rootScope = $injector.get('$rootScope');
            var crAcl = $injector.get('crAcl');

            switch (crAcl.getRole()) {
                case 'ROLE_GUEST' : $state.go('main.auth.signup');
                    break;
                case 'ROLE_DRIVER' : $state.go('main.search');
                    break;
                case 'ROLE_ADMIN' : $state.go('admin.drivers');
                    break;
            }
            
        });
 
        $stateProvider 
            .state('main', {
                url: '/',
                templateUrl: '../views/main.html',
                abstract: true
            })
            .state('main.sitemap', {
                url: 'sitemap',
                templateUrl: '../views/sitemap.html'
            });

        NotificationProvider.setOptions({
            replaceMessage: true
        });
    }

    run.$inject = ['$rootScope', '$cookieStore', '$http', 'crAcl', 'MemberShipService', '$state'];
    function run($rootScope, $cookieStore, $http, crAcl, MemberShipService, $state) {

        // keep user logged in after page refresh
        $rootScope.globals = $cookieStore.get('globals') || {};

        crAcl
            .setInheritanceRoles({
                'ROLE_DRIVER': ['ROLE_DRIVER', 'ROLE_GUEST'],
                'ROLE_ADMIN': ['ROLE_DRIVER', 'ROLE_ADMIN', 'ROLE_GUEST'],
                'ROLE_GUEST': ['ROLE_GUEST']
            });

        crAcl
            .setRedirect('main.auth.signup');

        if ($rootScope.globals.currentUser) {
            $http.defaults.headers.common['Authorization'] = $rootScope.globals.currentUser.token;
            crAcl.setRole($rootScope.globals.currentUser.role);
        }
        else {
            crAcl.setRole(); 
        }

        function getSubscriptionByDriverId() {
            function success(response) {
                if (!response.data.data.length)
                    $state.go('main.membership');
            }

            if (crAcl.getRole() === 'ROLE_DRIVER')
                MemberShipService
                    .getSubscriptionByDriverId($rootScope.globals.currentUser.driver._id)
                    .then(success);
        }

        $rootScope.$on('$stateChangeStart', getSubscriptionByDriverId);

    }

})();
 