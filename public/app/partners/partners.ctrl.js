(function () {
    'use strict';

    angular
        .module('main')
        .controller('PartnersCtrl', PartnersCtrl);

    function PartnersCtrl($log, CompaniesService, ngDialog, Notification) {
        var vm = this;


        vm.getPartnerCompanies = getPartnerCompanies;
        vm.companyDetailDialog = companyDetailDialog;
        
        vm.pcompanies = [];
        
        vm.params = {
            page: 1,
            limit: 9,
            city: null,
            zipcode: null,
            metroArea: null,
            empSizeRange: null,
            salseVolumeRange: null,
            isPartner :1 // getting only is partnered companies...
        };

        init();
        

        function init() {
			getPartnerCompanies();            
        }

        function getPartnerCompanies() {
            function success(response) {
                vm.pcompanies = response.data.data.companyData;
                vm.totalRecords = response.data.data.totalRecords;
            	console.log('VM PARAMS',vm.pcompanies);
            }
 
            function failed(response) {
                $log.error(response.data.message);
            }
            CompaniesService
                .companyFilter(vm.params)
                .then(success, failed);
        }

        function companyDetailDialog(company) {

            var options = {
                templateUrl: '../views/partners/partners.detail.html',
                showClose: false,
                appendClassName: 'ngdialog-default',
                controller: 'PartnersCtrl as vm',
                data: {
                    company: company
                }
            };

            ngDialog.open(options);
        }

        

    }

})();
 