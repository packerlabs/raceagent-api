(function () {
    'use strict';

    angular
        .module('main.partners', [])
        .config(config);

    config.$inject = ['$stateProvider', '$urlRouterProvider', '$httpProvider'];
    function config($stateProvider, $urlRouterProvider, $httpProvider) {
        
        $stateProvider
            .state('main.partners', {
                url: 'partners',
                templateUrl: '../views/partners/partners.html',
                controller: 'PartnersCtrl as vm',
                data: {
                    is_granted: ['ROLE_DRIVER']
                }
            });
        
    }

})();
 