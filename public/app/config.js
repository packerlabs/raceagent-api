angular.module("config", [])
    .constant("DRIVER_URL", "/api/driver")
    .constant("DRIVER_LIST_URL", "/api/driversList")
    .constant("DRIVER_CARD_DETAIL", "/api/driver/account/card")
    .constant("DRIVER_MEDIA_URL", "/api/driver/profilePicture")
    .constant("DRIVER_PLAN_URL", "/api/driver/plan/subscription")
    .constant("DRIVER_COMPANY_FAVORITE_URL", "/api/driver/company/favorite")
    .constant("COMPANY_FILTER_URL", "/api/company/filter")
    .constant("COMPANY_STATE_LIST_URL", "/api/company/stateList")
    .constant("COMPANY_IS_PARTNER_TOGGLE_URL", "/api/company/isPartner")
    .constant("COMPANY_PARTNER_LIST_URL", "/api/company/partnerList")
    .constant("COMPANY_METRO_AREA_LIST_URL", "/api/company/metroAreaList")
    .constant("COMPANY_EMPLOYEE_SIZE_LIST_URL", "/api/company/employeeSizeList")
    .constant("COMPANY_SALES_VOLUME_LIST_URL", "/api/company/salesVolumeList")
    .constant("PLAN_LIST_URL", "/api/plan")
    .constant("PLAN_SUBSCRIBE_URL", "/api/plan/subscription")
    .constant("PLAN_CHANGE_URL", "/api/plan/change")
    .constant("MESSAGE_URL", "/api/messages")
    .constant("UPLOAD_CSV_URL", "/api/uploadCsv")
    .constant("UPLOAD_PDF_URL", "/api/uploadPdf")
    .constant("DRIVER_PAYMENT_DETAIL_UPDATE", "/api/driver/account/card")
    .constant("SPONSOR_URL", "/api/sponsors");
  
    

