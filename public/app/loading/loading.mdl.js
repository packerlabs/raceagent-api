(function () {
    'use strict';

    angular
        .module('main.loading', [])
        .config(config);

    config.$inject = ['$stateProvider', '$urlRouterProvider', '$httpProvider'];
    function config($stateProvider, $urlRouterProvider, $httpProvider) {
        
        $stateProvider
            .state('main.loading', {
                url: 'loading',
                templateUrl: '../views/loading/loading.html',
                controller: 'LoadingCtrl as vm'
            });
        
    }

})();
 