(function () {
    'use strict';

    angular
        .module('main')
        .controller('LoadingCtrl', LoadingCtrl);

    function LoadingCtrl($interval, $state, $http) {
        var vm = this;
        var i = 0;
        var iSum = 0;

        vm.style = {
            'background-image': null
        };

        vm.steps = [
            {
                name: 'READY',
                img: '../dist/images/ready.png'
            },
            {
                name: 'SET',
                img: '../dist/images/set.png'
            },
            {
                name: 'GO',
                img: '../dist/images/go.png'
            }
        ];

        preloadImages(vm.steps);
        vm.style = {
            'background-image': 'url(' + preloadImages.cache[0].src + ')'
        };

        function preloadImages() {
            if (!preloadImages.cache) {
                preloadImages.cache = [];
            }
            var img;
            vm.steps.forEach(function (item) {
                img = new Image();
                img.src = item.img;
                preloadImages.cache.push(img);
            });
        }

        console.log(preloadImages.cache);
        function loading() {
            if (i < 3 && iSum < 36) {
                vm.step = vm.steps[i];
                vm.style['background-image'] = 'url(' + preloadImages.cache[i].src + ')';
                vm.style.opacity = 1;
                i++;
                iSum++;
            } else if (i > 0) {
                i = 0;
            }
        }

        $interval(loading, 1500);
    }

})();
 