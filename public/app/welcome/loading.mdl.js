(function () {
    'use strict';

    angular
        .module('main.welcome', [])
        .config(config); 

    config.$inject = ['$stateProvider', '$urlRouterProvider', '$httpProvider'];
    function config($stateProvider, $urlRouterProvider, $httpProvider) {
        
        $stateProvider
            .state('main.welcome', {
                url: 'welcome',
                templateUrl: '../views/welcome/welcome.html'
            });
        
    }

})();
 