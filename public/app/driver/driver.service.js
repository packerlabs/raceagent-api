(function () {
    'use strict';

    angular
        .module('main')
        .service('DriverService', function ($http, $q, $rootScope, DRIVER_URL, DRIVER_MEDIA_URL, DRIVER_LIST_URL, DRIVER_COMPANY_FAVORITE_URL,DRIVER_CARD_DETAIL,DRIVER_PAYMENT_DETAIL_UPDATE,DRIVER_PLAN_URL,PLAN_LIST_URL,PLAN_CHANGE_URL) {
            this.getCard = function(){
                return $http.get(DRIVER_CARD_DETAIL);
            };

            this.updatePaymentDetails = function(cardId,data){
                return $http.put(DRIVER_PAYMENT_DETAIL_UPDATE + '/' + cardId, data);
            };
            
            this.getSubscribedPlan = function(){
                return $http.get(DRIVER_PLAN_URL);
            };

            this.getDriverPlanList = function () {
                return $http.get(PLAN_LIST_URL);
            };

            this.changePlan = function(plan){
                return $http.put(PLAN_CHANGE_URL,plan);
            };

            this.register = function (driver) {
                return $http.post(DRIVER_URL, driver);
            };

            this.getProfile = function (id) {
                return $http.get(DRIVER_URL + '/' + id);
            };

            this.getProfileByUsername = function (username) {
                return $http.get(DRIVER_URL + '/username/' + username);
            };

            this.getFavoritesCompanies = function (params) {
                return $http.get(DRIVER_COMPANY_FAVORITE_URL, {
                    params: params
                });
            };

            this.removeFavoriteCompany = function (companyId) {
                return $http.delete(DRIVER_COMPANY_FAVORITE_URL + '/' + companyId);
            };

            this.addCompanyToDriverFavoriteList = function (companyId) {
                return $http.post(DRIVER_COMPANY_FAVORITE_URL, {
                    companyId: companyId
                });
            };

            this.login = function (driver) {
                return $http.post(DRIVER_URL + '/login', driver);
            };

            this.update = function (driver) {
                return $http.put(DRIVER_URL + '/' + driver._id, driver);
            };

            this.getAll = function () {
                return $http.get(DRIVER_LIST_URL);
            };

            this.delete = function (id) {
                return $http.delete(DRIVER_URL + '/' + id);
            };

            this.forgotPassword = function (email) {
                return $http.post(DRIVER_URL + '/forgotPassword', { email: email });
            };

            this.resetPassword = function (data) {
                return $http.post(DRIVER_URL + '/resetPassword', data);
            };

            this.verifyDriverAccount = function (data) {
              return $http.get(DRIVER_URL + '/verifyUserAccount/' + data.token);
            }

            this.uploadAvatar = function (base64) {

                var defer = $q.defer();

                var mimeType = /data\:([^)]+)\;/.exec(base64)[1];
                var fileName = 'avatar.' + (/\/(.*)/.exec(mimeType)[1]);

                convertBase64ToFile(base64, fileName, mimeType)
                    .then(function(file){
                        var fd = new FormData();

                        fd.append('file', file);

                        var xhttp = new XMLHttpRequest();

                        xhttp.upload.addEventListener("progress",function (e) {
                            defer.notify(parseInt(e.loaded * 100 / e.total));
                        });
                        xhttp.upload.addEventListener("error",function (e) {
                            defer.reject(e);
                        });

                        xhttp.onreadystatechange = function() {
                            if (xhttp.readyState === 4) {
                                defer.resolve(JSON.parse(xhttp.response)); //Outputs a DOMString by default
                            }
                        };

                        xhttp.open("post", DRIVER_MEDIA_URL, true);
                        xhttp.setRequestHeader('Authorization', $rootScope.globals.currentUser.token);

                        xhttp.send(fd);
                    });

                return defer.promise;

            };

            function convertBase64ToFile(url, filename, mimeType) {
                return (fetch(url)
                        .then(function(res){return res.arrayBuffer();})
                        .then(function(buf){return new File([buf], filename, {type:mimeType});})
                );
            }

        });
})();
