(function () {
    'use strict';

    angular
        .module('main.driver.account.uploadAvatar', [])
        .config(config);

    config.$inject = ['$stateProvider', '$urlRouterProvider', '$httpProvider'];
    function config($stateProvider, $urlRouterProvider, $httpProvider) {
        
        $stateProvider
            .state('main.driver.account.uploadAvatar', {
                url: '/upload-avatar',
                onEnter: function (ngDialog, $state) {
                    var options = {
                        templateUrl: '../views/driver/account/driver.account.uploadAvatar.html',
                        showClose: true,
                        controller: 'DriverAccountCtrl as vm',
                        appendClassName: 'ngdialog-default'
                    };

                    ngDialog 
                        .open(options)
                        .closePromise
                        .then(function (e) {
                            if (e.value)
                                $state.go('main.driver.account');
                            
                            return true;
                    });
                },
                data: {
                    is_granted: ['ROLE_DRIVER']
                }
            });
        
    }

})();
 