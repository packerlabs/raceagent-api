(function () {
    'use strict';

    angular
        .module('main')
        .controller('DriverAccountCtrl', DriverAccountCtrl);

    function DriverAccountCtrl(DriverService,
                               AuthService,
                               Notification,
                               MemberShipService,
                               ngDialog,
                               $scope,
                               $rootScope,
                               $state,
                               DRIVER_MEDIA_URL,
                               UPLOAD_PDF_URL) {
        var vm = this;

        vm.formDriver = null;
        vm.subscriptionDate = null;

        vm.driver = {};
        vm.name = 'name';
        vm.flow = {};
        vm.flowConfig = {
            target: DRIVER_MEDIA_URL,
            singleFile: true,
            testChunks: false
        };

        vm.flowPdf = {};
        vm.flowPdfConfig = {
            target: UPLOAD_PDF_URL,
            singleFile: true,
            testChunks: false
        };

        vm.seriesTypes = [
            {
                name: 'test1',
                value: 1
            },
            {
                name: 'test2',
                value: 2
            },
            {
                name: 'test3',
                value: 3
            },
            {
                name: 'test4',
                value: 4
            },
            {
                name: 'test5',
                value: 5
            }
        ];
        vm.uploadProgress = 0;

        vm.rows = [
            {
                seriesType: 1,
                carNumber: 554654
            }
        ];

        vm.originImage = '';
        vm.profilePic = '';

        vm.getDriver = getDriver;
        vm.update = update;
        vm.addRow = addRow;
        vm.fileAdded = fileAdded;
        vm.upload = upload;
        vm.rotate = rotate;
        vm.getSubscription = getSubscription;
        vm.updatePaymentDetails = updatePaymentDetails;

        function fileAdded($flow, $file) {
            var reader = new FileReader();

            reader.onload = function (evt) {
                $scope.$apply(function($scope){
                    $scope.originImage = evt.target.result;
                });
            };
            reader.readAsDataURL($file.file);
        }

        function addRow() {
            vm.rows.push({
                seriesType: null,
                carNumber: null
            });
        }

        //alert('here i call driver');

        function getDriver() {

            function success(response) {
                vm.driver = response.data.data;

                vm.driver.birthdate = new Date(vm.driver.birthdate);
            }

            function failed(response) {
                Notification.error(response.data.message);
            }

            DriverService
                .getProfile(AuthService.getCurrentUser().driver._id)
                .then(success, failed);

             DriverService
                .getCard().then(function(response){
                    var m = 1;
                    var data = response.data[0];
                    var cDetails = data.cardId[0];
                    var m = cDetails.expiringMonth;
                    var y = cDetails.expiringYear;
                    cDetails.expireData = m +'/'+ getLastMonthDay(m,y)+'/'+ y;
                    
                    var pDetails = {
                                     'isPaymentApplied' : data.isPaymentApplied,
                                     'nextBillDate' : data.nextBillDate,
                                     'createdOn' : data.createdOn
                                    };
                    

                    vm.driver.card = cDetails;
                    vm.driver.payament = pDetails;

                    vm.formPayment = cDetails;
                });

            DriverService.getSubscribedPlan()
                .then(function(response){
                    
                    vm.subPlan = response.data.data.planId;
                    //console.log('->',response.message);
                });

            DriverService.getDriverPlanList()
                .then(function(response){
                    
                    vm.plansList = response.data.data;
                    //console.log('->',response.message);
                });
        }


         function updatePaymentDetails(){
            DriverService.updatePaymentDetails(vm.formPayment._id, vm.formPayment)
            .then(function(response){
                Notification.success('Updated');
                // console.log('->',response.message);
            });
            var newPlan = {planId : vm.subPlan._id};
            console.log('newPlan',newPlan);
            DriverService.changePlan(newPlan)
            .then(function(response){
                //Notification.success('Updated plan');
                // console.log('->',response.message);
            });

            
            console.log('hello worl',vm.formPayment,vm.subPlan._id);    
        } 


        function getLastMonthDay(m,y){
            var thirtyOneDays = [1,3,5,7,8,10,12];
            var thirtyDays = [4,6,9,11];
            
                    
            if(thirtyOneDays.indexOf(m) >= 0)
            {
                return 31;            
            }
            else
            {
                if(thirtyDays.indexOf(m) >= 0){
                    return 30
                }else
                {

                   if(m == 2){
                        if(leapYear(y)){

                            return 29;

                        }else{
                            return 28;    
                        }
                   } 
                }
            }
            return 0;
        }

        function leapYear(year)
        {
          return ((year % 4 == 0) && (year % 100 != 0)) || (year % 400 == 0);
        }

        function update(image, pdf) {
            function success() {
                Notification.success('Updated');
                vm.flow.cancel();
                ngDialog.close();
                $state.go('main.driver.account', {}, {reload: $state.is('main.driver.account.uploadAvatar')})
            }

            function failed(response) {
                Notification.error(response.data.message);
            }
            
            if (image)
                vm.driver.profilePic = image.fileName;

            if (pdf)
                vm.driver.sponsorshipPitchAttachment = JSON.parse(pdf).fileName;

            console.log($state.is('main.driver.account') &&
                vm.formDriver.$valid);

            if ($state.is('main.driver.account') &&
                vm.formDriver.$valid)
                DriverService
                    .update(vm.driver)
                    .then(success, failed);
            else if ($state.is('main.driver.account.uploadAvatar'))
                DriverService
                    .update(vm.driver)
                    .then(success, failed);
        }

        function getSubscription() {
            function success(response) {
                vm.subscriptionDate = new Date(response.data.data[0].createdOn).getDate();
            }

            MemberShipService
                .getSubscriptionByDriverId($rootScope.globals.currentUser.driver._id)
                .then(success);
        }

        function update(image, pdf) {
            function success() {
                Notification.success('Updated');
                vm.flow.cancel();
                ngDialog.close();
                $state.go('main.driver.account', {}, { reload: true });
            }

            function failed(response) {
                Notification.error(response.data.message);
            }
            
            if (image)
                vm.driver.profilePic = image.fileName;

            if (pdf)
                vm.driver.sponsorshipPitchAttachment = JSON.parse(pdf).fileName;

            console.log($state.is('main.driver.account') &&
                vm.formDriver.$valid);

            if ($state.is('main.driver.account') &&
                vm.formDriver.$valid || pdf)
                DriverService
                    .update(vm.driver)
                    .then(success, failed);
            else if ($state.is('main.driver.account.uploadAvatar'))
                DriverService
                    .update(vm.driver)
                    .then(success, failed);
        }

        function upload() {
            function success(data) {
                update(data);
            }

            function failed() {

            }
            
            function progress(progress) {
                vm.uploadProgress = progress;
            }
            
            DriverService
                .uploadAvatar(vm.profilePic)
                .then(success, failed, progress);
        }

        function rotate() {
            var canvas = document.createElement("canvas");
            var img = new Image();
            var cw = img.width, ch = img.height, cx = 0, cy = 0, deg = 0;

            img.onload = function() {
                var context = canvas.getContext("2d");

                cw = img.height;
                ch = img.width;
                cx =- img.width;
                deg = 270;

                canvas.width = cw;
                canvas.height = ch;

                context.rotate(deg * Math.PI / 180);
                context.drawImage(this, cx, cy);

                $scope.$apply(function($scope){
                    $scope.originImage = canvas.toDataURL("image/png");
                });

            };

            img.src = $scope.originImage;

        }

    }

})();
 