(function () {
    'use strict';

    angular
        .module('main.driver.account', [
            'main.driver.account.uploadAvatar'
        ])
        .config(config);

    config.$inject = ['$stateProvider', '$urlRouterProvider', '$httpProvider'];
    function config($stateProvider, $urlRouterProvider, $httpProvider) {
        
        $stateProvider
            .state('main.driver.account', {
                url: 'account',
                views: {
                    '@main': {
                        templateUrl: '../views/driver/account/driver.account.html',
                        controller: 'DriverAccountCtrl as vm'
                    }
                },
                data: {
                    is_granted: ['ROLE_DRIVER']
                }
            });
        
    }

})();
 