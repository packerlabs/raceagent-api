(function () {
    'use strict';

    angular
        .module('main.driver', [ 
            'main.driver.account',
            'main.driver.profile'
        ])
        .config(config);

    config.$inject = ['$stateProvider', '$urlRouterProvider', '$httpProvider'];
    function config($stateProvider, $urlRouterProvider, $httpProvider) {
        
        $stateProvider
            .state('main.driver', {
                url: 'driver/',
                abstract: true
            });
        
    }

})();
 