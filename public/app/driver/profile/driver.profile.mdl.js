(function () {
    'use strict';

    angular
        .module('main.driver.profile', [])
        .config(config);

    config.$inject = ['$stateProvider', '$urlRouterProvider', '$httpProvider'];
    function config($stateProvider, $urlRouterProvider, $httpProvider) {
        
        $stateProvider
            .state('main.driver.profile', {
                url: ':username',
                views: {
                    '@main': {
                        templateUrl: '../views/driver/profile/driver.profile.html',
                        controller: 'DriverProfileCtrl as vm'
                    }
                }
            });
        
    }

})();
 