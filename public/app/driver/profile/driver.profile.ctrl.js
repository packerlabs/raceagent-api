(function () {
    'use strict';

    angular
        .module('main')
        .controller('DriverProfileCtrl', DriverProfileCtrl);

    function DriverProfileCtrl(DriverService,
                               $stateParams,
                               Notification,
                               ngDialog,
                               $scope,
                               $state,
                               DRIVER_MEDIA_URL,
                               UPLOAD_PDF_URL) {
        var vm = this;

        vm.formDriver = null;

        vm.driver = {};

        vm.flow = {};
        vm.flowConfig = {
            target: DRIVER_MEDIA_URL,
            singleFile: true,
            testChunks: false
        };

        vm.flowPdf = {};
        vm.flowPdfConfig = {
            target: UPLOAD_PDF_URL,
            singleFile: true,
            testChunks: false
        };

        vm.seriesTypes = [
            {
                name: 'test1',
                value: 1
            },
            {
                name: 'test2',
                value: 2
            },
            {
                name: 'test3',
                value: 3
            },
            {
                name: 'test4',
                value: 4
            },
            {
                name: 'test5',
                value: 5
            }
        ];

        vm.uploadProgress = 0;
        vm.uploadPdfProgress = 0;

        vm.rows = [
            {
                seriesType: 1,
                carNumber: 554654
            }
        ];

        vm.originImage = '';
        vm.profilePic = '';

        vm.getDriver = getDriver;
        vm.update = update;
        vm.addRow = addRow;
        vm.fileAdded = fileAdded;
        vm.upload = upload;
        vm.rotate = rotate;
        vm.uploadPdf = uploadPdf;

        function fileAdded($flow, $file) {
            var reader = new FileReader();

            reader.onload = function (evt) {
                $scope.$apply(function($scope){
                    $scope.originImage = evt.target.result;
                });
            };
            reader.readAsDataURL($file.file);
        }

        function addRow() {
            vm.rows.push({
                seriesType: null,
                carNumber: null
            });
        }

        function getDriver() {
            function success(response) {
                vm.driver = response.data.data;

                vm.driver.birthdate = new Date(vm.driver.birthdate);
            }

            function failed(response) {
                Notification.error(response.data.message);
            }

            DriverService
                .getProfileByUsername($stateParams.username)
                .then(success, failed);
        }

        function update(image) {
            function success() {
                Notification.success('Updated');
                vm.flow.cancel();
                ngDialog.close();
                $state.go('main.driver.profile', {}, {reload: $state.is('main.driver.profile.uploadAvatar')})
            }

            function failed(response) {
                Notification.error(response.data.message);
            }
            
            if (image)
                vm.driver.profilePic = image.fileName;

            if ($state.is('main.driver.profile') &&
                vm.formDriver.$valid)
                DriverService
                    .update(vm.driver)
                    .then(success, failed);
            else if ($state.is('main.driver.profile.uploadAvatar'))
                DriverService
                    .update(vm.driver)
                    .then(success, failed);
        }

        function upload() {
            function success(data) {
                update(data);
            }

            function failed() {

            }
            
            function progress(progress) {
                vm.uploadProgress = progress;
            }
            
            DriverService
                .uploadAvatar(vm.profilePic)
                .then(success, failed, progress);
        }

        function rotate() {
            var canvas = document.createElement("canvas");
            var img = new Image();
            var cw = img.width, ch = img.height, cx = 0, cy = 0, deg = 0;

            img.onload = function() {
                var context = canvas.getContext("2d");

                cw = img.height;
                ch = img.width;
                cx =- img.width;
                deg = 270;

                canvas.width = cw;
                canvas.height = ch;

                context.rotate(deg * Math.PI / 180);
                context.drawImage(this, cx, cy);

                $scope.$apply(function($scope){
                    $scope.originImage = canvas.toDataURL("image/png");
                });

            };

            img.src = $scope.originImage;

        }

        function uploadPdf($file) { 
            function success(data) {
                update(data);
            }

            function failed() {

            }

            function progress(progress) {
                vm.uploadPdfProgress = progress;
            }

            // DriverService
            //     .uploadPdf($file)
            //     .then(success, failed, progress);
            vm.flowPdf.upload();
        }

    }

})();
 