(function () {
    'use strict';

    angular
        .module('main.membership', [])
        .config(config);

    config.$inject = ['$stateProvider', '$urlRouterProvider', '$httpProvider'];
    function config($stateProvider, $urlRouterProvider, $httpProvider) {
        
        $stateProvider
            .state('main.membership', {
                url: 'membership',
                templateUrl: '../views/membership/membership.html',
                controller: 'MemberShipCtrl as vm'
            });
        
    }

})();
 