(function () {
    'use strict';

    angular
        .module('main')
        .controller('MemberShipCtrl', MemberShipCtrl);

    function MemberShipCtrl(MemberShipService, $rootScope, $state) {
        var vm = this;

        init(); 

        vm.plans = [];
        vm.plan = {};
        
        vm.subscribePlan = subscribe;
        vm.getPlanList = getPlanList;

        var handler = StripeCheckout.configure({
            key: 'pk_live_2DfJupVZQEbB7MJ8P9qKrYPJ',
            image: 'dist/images/logo.png',
            locale: 'auto',
            token: function(token) {
                vm.plan.stripeToken = token.id;
                vm.plan.email = token.email;
                
                MemberShipService
                    .subscribePlan(vm.plan)
                    .then(function(data) {
                        $state.go('main.welcome');
                });
            }
        });

        function init() {
            getPlanList();
        }
        
        function getPlanList() {
            function success(response) {
                vm.plans = response.data.data;
            } 

            function error(response) {
                vm.plans = response.data.data;
            }

            MemberShipService
                .getPlanList()
                .then(success, error);
        }

        function subscribe(amount, name, planId) {
            vm.plan.planId = planId;

            handler.open({
                name: name ? name + ' Membership' : 'Membership',
                description: '',
                amount: amount
            });
        }
    }

})();
 