(function () {
    'use strict';

    angular
        .module('main')
        .service('MemberShipService', function ($http, PLAN_SUBSCRIBE_URL, PLAN_LIST_URL, $q, crAcl) {

            this.getPlanList = function () {
                return $http.get(PLAN_LIST_URL);
            };

            this.subscribePlan = function (planObject) {
                return $http.post(PLAN_SUBSCRIBE_URL, planObject);
            };

            this.getSubscriptionByDriverId = function (driverId) {
                return $http.get(PLAN_SUBSCRIBE_URL, {
                    params: {
                        driverId: driverId
                    }
                });
            };
        });  
})();   