(function () {
    'use strict';

    angular
        .module('main')
        .controller('MailboxCtrl', MailboxCtrl);
 
    function MailboxCtrl(MailboxService, Notification, $rootScope) {
        var vm = this;

        vm.messages = {};
        vm.tab = 'inbox';

        vm.getMessages = getMessages;
        vm.openMessage = openMessage;
        vm.setTab = setTab;

        function getMessages() {
            function success(response) {
                vm.messages = response.data;
            }

            function failed(response) {
                Notification.error(response.data.message);
            }

            MailboxService
                .getMessages($rootScope.globals.currentUser.driver.email)
                .then(success, failed);
        }

        function openMessage($index) {
            vm.message = vm.messages[vm.tab][$index];
        }
        
        function setTab(tab) {
            vm.tab = tab;
            vm.message = null;
        }

    }

})();
 