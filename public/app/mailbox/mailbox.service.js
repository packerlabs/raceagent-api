(function () {
    'use strict';

    angular
        .module('main')
        .service('MailboxService', function ($http, MESSAGE_URL) {

            this.getAllMessages = function () {
                return $http.get(MESSAGE_URL);
            };
            
            this.getMessages = function (userId) {
                return $http.get(MESSAGE_URL + '/' + userId);
            };

            this.sendMessagesToUser = function (message) {
                return $http.post(MESSAGE_URL, message);
            };

        });  
})();  