(function () {
    'use strict';

    angular
        .module('main.mailbox', [])
        .config(config);

    config.$inject = ['$stateProvider'];
    function config($stateProvider) {
        
        $stateProvider
            .state('main.mailbox', {
                url: 'emails',
                templateUrl: '../views/mailbox/mailbox.html',
                controller: 'MailboxCtrl as vm',
                data: {
                    is_granted: ['ROLE_DRIVER']
                }
            });
        
    }
 
})();
 