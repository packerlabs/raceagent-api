(function () {
    'use strict';

    angular
        .module('main.search', [])
        .config(config);

    config.$inject = ['$stateProvider', '$urlRouterProvider', '$httpProvider'];
    function config($stateProvider, $urlRouterProvider, $httpProvider) {
        
        $stateProvider
            .state('main.search', {
                url: 'search',
                templateUrl: '../views/search/search.html',
                controller: 'SearchCtrl as vm'
            });
        
    }
 
})();
 