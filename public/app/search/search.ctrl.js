(function () {
    'use strict';

    angular
        .module('main')
        .controller('SearchCtrl', SearchCtrl);

    function SearchCtrl($log, $state, CompaniesService) {
        var vm = this;

        vm.params = {
            city: null,
            zipcode: null,
            metroArea: null,
            empSizeRange: null,
            salseVolumeRange: null
        };

        vm.init = function () {
            getStateList();
            getMetroAreaList();
            getEmployeeSizeList();
            getSalesVolumeList();
        };

        vm.getCompaniesByParams = getCompaniesByParams;

        function getCompaniesByParams() {
            $state.go('main.companies', vm.params);
        }

        function getSalesVolumeList() {
            function success(response) {
                vm.salesVolumeList = response.data.data;
            }

            function failed(response) {
                $log.error(response.data.message);
            }

            CompaniesService
                .getSalesVolumeList()
                .then(success, failed);
        }

        function getStateList() {
            function success(response) {
                vm.stateList = response.data.data;
            }

            function failed(response) {
                $log.error(response.data.message);
            }

            CompaniesService
                .getStateList()
                .then(success, failed);
        }

        function getMetroAreaList() {
            function success(response) {
                vm.metroAreaList = response.data.data;
            }

            function failed(response) {
                $log.error(response.data.message);
            }

            CompaniesService
                .getMetroAreaList()
                .then(success, failed);
        }

        function getEmployeeSizeList() {
            function success(response) {
                vm.employeeSizeList = response.data.data;
            }

            function failed(response) {
                $log.error(response.data.message);
            }

            CompaniesService
                .getEmployeeSizeList()
                .then(success, failed);
        }

    }

})();
 