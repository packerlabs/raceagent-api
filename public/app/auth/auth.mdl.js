(function () {
    'use strict';

    angular
        .module('main.auth', [
            'main.auth.signup',
            'main.auth.signin',
            'main.auth.forgotPassword',
            'main.auth.resetPassword',
            'main.auth.verifyAccount'
        ])
        .config(config);

    config.$inject = ['$stateProvider', '$urlRouterProvider', '$httpProvider'];
    function config($stateProvider, $urlRouterProvider, $httpProvider) {

        $stateProvider
            .state('main.auth', {
                url: '',
                abstract: true
            });

    }

})();
