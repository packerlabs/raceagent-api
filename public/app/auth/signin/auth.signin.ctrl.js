(function () {
    'use strict';

    angular
        .module('main')
        .controller('AuthSignInCtrl', AuthSignInCtrl);

    function AuthSignInCtrl(DriverService, AuthService, MemberShipService, Notification, crAcl, $state, $stateParams) {
        var vm = this;

        vm.loginForm = null;

        vm.credentials = {}; 
        
        vm.alert = null;

        vm.login = login;
        
        init();
        
        function init() {
            if ($stateParams.verifyUserAccount)
                verifyUserAccount();
        }

        function login() {
            function success(response) {
                response.data.data.role = 'ROLE_DRIVER';

                AuthService
                    .setCredentials(response.data.data);
                
                crAcl
                    .setRole('ROLE_DRIVER');

                getSubscriptionByDriverId(response.data.data.driver._id)
            }

            function failed(response) {
                Notification.error(response.data.message);
            }

            if (vm.loginForm.$valid)
                DriverService
                    .login(vm.credentials)
                    .then(success, failed);
        }

        function getSubscriptionByDriverId(id) {
            function success(response) {
                if (response.data.data.length)
                    $state.go('main.search');
                else
                    $state.go('main.membership');
            }

            MemberShipService
                .getSubscriptionByDriverId(id)
                .then(success);
        }

        function verifyUserAccount() {
            function success(response) {
                vm.alert = {
                    type: 'success',
                    text: response.data.message
                }
            }
            
            function error(response) {
                vm.alert = {
                    type: 'error',
                    text: response.data.message
                }
            }

            AuthService
                .verifyUserAccount($stateParams.verifyUserAccount)
                .then(success, error);
        }
        
        

    }

})();
 