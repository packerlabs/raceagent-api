(function () {
    'use strict';

    angular
        .module('main.auth.signin', [])
        .config(config);

    config.$inject = ['$stateProvider', '$urlRouterProvider', '$httpProvider'];
    function config($stateProvider, $urlRouterProvider, $httpProvider) {
        
        $stateProvider
            .state('main.auth.signin', {
                url: 'signin?verifyUserAccount',
                views: {
                    '@main': { 
                        templateUrl: '../views/auth/auth.signin.html',
                        controller: 'AuthSignInCtrl as vm'
                    }
                }
            });
        
    }

})();
 