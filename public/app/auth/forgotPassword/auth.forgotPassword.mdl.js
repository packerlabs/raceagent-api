(function () {
    'use strict';

    angular
        .module('main.auth.forgotPassword', [])
        .config(config);

    config.$inject = ['$stateProvider', '$urlRouterProvider', '$httpProvider'];
    function config($stateProvider, $urlRouterProvider, $httpProvider) {
        
        $stateProvider
            .state('main.auth.forgotPassword', {
                url: 'forgotPassword',
                views: {
                    '@main': {
                        templateUrl: '../views/auth/auth.forgotPassword.html',
                        controller: 'AuthForgotPasswordCtrl as vm'
                    }
                }
            });
        
    }

})();
 