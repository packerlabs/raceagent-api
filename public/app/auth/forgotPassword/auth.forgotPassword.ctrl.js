(function () {
    'use strict';

    angular
        .module('main')
        .controller('AuthForgotPasswordCtrl', AuthForgotPasswordCtrl);

    function AuthForgotPasswordCtrl(DriverService, Notification) {
        var vm = this;

        vm.send = send;

        function send() {
            function success(response) {
                Notification.success(response.data.message);
            }

            function failed(response) {
                Notification.error(response.data.message);
            }

            DriverService
                .forgotPassword(vm.email)
                .then(success, failed);
        }
        
    }

})();
 