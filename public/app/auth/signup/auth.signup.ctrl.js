(function () {
    'use strict';

    angular
        .module('main')
        .controller('AuthSignUpCtrl', AuthSignUpCtrl);

    function AuthSignUpCtrl(DriverService, Notification, AuthService, crAcl, $state) {
        var vm = this;

        vm.registerForm = null;
        vm.usernameIsExist = false;
        vm.checkUsernameShow = false;

        vm.driver = {};

        vm.register = register;
        vm.checkUsername = checkUsername;

        function register() {
            function success() {
                login();
            } 

            function failed(response) {
                if (response.status === 400)
                    switch (response.data.code) {
                        case 11000 : Notification.error('This email is already exist'); break;
                    }
                else
                    Notification.error(response.data.message);
            }

            vm.driver.birthdate = new Date();

            if (vm.registerForm.$valid)
                if (vm.driver.password === vm.driver.verifyPassword
                    && !vm.usernameIsExist)
                    DriverService
                        .register(vm.driver)
                        .then(success, failed);

                else if (vm.driver.password !== vm.driver.verifyPassword)
                    Notification.error('Verify Password isn\'t correct');

                else if (vm.usernameIsExist)
                    Notification.error('Username already used');
        }

        function login() {
            function success(response) {
                response.data.data.role = 'ROLE_DRIVER';

                AuthService
                    .setCredentials(response.data.data);

                crAcl
                    .setRole('ROLE_DRIVER');

                $state.go('main.search');
            }

            function failed(response) {
                Notification.error(response.data.message);
            }

            DriverService
                .login(vm.driver)
                .then(success, failed);
        }
        
        function checkUsername() {
            function success(response) {
                vm.usernameIsExist = !!response.data.data;
                vm.checkUsernameShow = true;

                console.log(vm.usernameIsExist);
            }

            function failed(response) {
            }

            if (vm.driver.userName)
                DriverService
                    .getProfileByUsername(vm.driver.userName)
                    .then(success, failed);
            else
                vm.checkUsernameShow = false;
        }

    }

})();
 