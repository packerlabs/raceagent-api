(function () {
    'use strict';

    angular
        .module('main.auth.signup', [])
        .config(config);

    config.$inject = ['$stateProvider', '$urlRouterProvider', '$httpProvider'];
    function config($stateProvider, $urlRouterProvider, $httpProvider) {
        
        $stateProvider
            .state('main.auth.signup', {
                url: 'signup',
                views: {
                    '@main': {
                        templateUrl: '../views/auth/auth.signup.html',
                        controller: 'AuthSignUpCtrl as vm'
                    }
                },
                onEnter: function (AuthService) {
                    AuthService.clearCredentials();
                }
            });
        
    }

})();
 