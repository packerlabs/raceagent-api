(function () {
    'use strict';

    angular
        .module('main')
        .service('AuthService', function ($http, $cookieStore, $rootScope, $q, crAcl) {

            var urlBase = '/api/';

            var authService = this;
            
            authService.login = function (credentials) {
                return $http.post(urlBase + 'admin/login', credentials);
            };
            
            authService.setCredentials = function (user) {
                $rootScope.globals = {
                    currentUser: user
                };

                $http.defaults.headers.common['Authorization'] = user.token;
                $cookieStore.put('globals', $rootScope.globals);
            };

            authService.clearCredentials = function () {
                var deferred = $q.defer();
                $cookieStore.remove('globals');

                if (!$cookieStore.get('globals')) {
                    $rootScope.globals = {};
                    $http.defaults.headers.common.Authorization = '';
                    crAcl.setRole();
                    deferred.resolve('Credentials clear success');
                } else {
                    deferred.reject('Can\'t clear credentials');
                }

                return deferred.promise;
            };

            authService.getCurrentUser = function () {
                return $rootScope.globals.currentUser;
            };
            
            authService.verifyUserAccount = function (driverId) {
                return $http.get(urlBase + 'driver/verifyUserAccount/' + driverId);
            }

        });  
})();  