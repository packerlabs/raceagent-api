(function () {
    'use strict';

    angular
        .module('main.auth.resetPassword', [])
        .config(config);

    config.$inject = ['$stateProvider', '$urlRouterProvider', '$httpProvider'];
    function config($stateProvider, $urlRouterProvider, $httpProvider) {
        
        $stateProvider
            .state('main.auth.resetPassword', {
                url: 'resetPassword?token',
                views: {
                    '@main': {
                        templateUrl: '../views/auth/auth.resetPassword.html',
                        controller: 'ResetPasswordPasswordCtrl as vm'
                    }
                }
            });
        
    }

})();
 