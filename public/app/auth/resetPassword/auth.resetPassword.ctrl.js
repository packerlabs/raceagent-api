(function () {
    'use strict';

    angular
        .module('main')
        .controller('ResetPasswordPasswordCtrl', ResetPasswordPasswordCtrl);

    function ResetPasswordPasswordCtrl(DriverService, Notification, $stateParams) {
        var vm = this;

        vm.data = {
            token: $stateParams.token,
            password: null
        };
        
        vm.reset = reset;

        function reset() {
            function success(response) {
                Notification.success(response.data.message);
            }

            function failed(response) {
                Notification.error(response.data.message);
            }

            DriverService
                .resetPassword(vm.data)
                .then(success, failed);
        }
        
    }

})();
 