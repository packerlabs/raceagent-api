(function () {
  'use strict';

  angular
      .module('main.auth.verifyAccount', [])
      .config(config);

  config.$inject = ['$stateProvider', '$urlRouterProvider', '$httpProvider'];
  function config($stateProvider, $urlRouterProvider, $httpProvider) {

      $stateProvider
          .state('main.auth.verifyAccount', {
              url: 'verifyAccount/:token',
              views: {
                  '@main': {
                      templateUrl: '../views/auth/auth.verifyAccount.html',
                      controller: 'verifyAccountCtrl as vm'
                  }
              }
          });

  }

})();
