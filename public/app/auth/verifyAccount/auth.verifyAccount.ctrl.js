(function () {
  'use strict';

  angular
    .module('main')
    .controller('verifyAccountCtrl', verifyAccountCtrl);

  function verifyAccountCtrl(DriverService, Notification, $stateParams, $state) {
    var vm = this;

    vm.data = {
      token: $stateParams.token
    };

    vm.redirectToLogin = redirectToLogin;
    vm.verifyUserAccount = verifyUserAccount;
    vm.message = "Verification is in progress";
    function redirectToLogin() {
      $state.go('main.auth.signin');
    };

    function verifyUserAccount() {
      function success(response) {
        vm.message = "Your account has been successfully verified";
        Notification.success(response.data.message);
      }

      function failed(response) {
        vm.message = "Error while verifying your account";
        Notification.error(response.data.message);
      }

      DriverService
        .verifyDriverAccount(vm.data)
        .then(success, failed);
    }

  }

})();
