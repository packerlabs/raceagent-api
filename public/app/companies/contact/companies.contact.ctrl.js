(function () {
    'use strict';

    angular
        .module('main')
        .controller('CompaniesContactCtrl', CompaniesContactCtrl);

    function CompaniesContactCtrl($log, $rootScope, $scope, ngDialog, MailboxService, CompaniesService, DriverService, Notification) {
        var vm = this;

        vm.message = {
            senderMailId: $rootScope.globals.currentUser.driver.email,
            receiverMailId: $scope.ngDialogData.company['EXECUTIVE EMAIL'],
            subject: $scope.ngDialogData.company['COMPANY NAME'] + ' - ' + $rootScope.globals.currentUser.driver.firstName + ' ' + $rootScope.globals.currentUser.driver.lastName,
            messageContents: null,
            profileAttached: false
        };

        vm.send = sendMessage;

        function sendMessage() {
            function success(response) {
                Notification.success(response.data.message);
                ngDialog.close();
            }

            function failed(response) {
                Notification.error(response.data.message);
            }

            MailboxService
                .sendMessagesToUser(vm.message)
                .then(success, failed);
        }

    }

})();
