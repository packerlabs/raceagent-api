(function () {
    'use strict';

    angular
        .module('main')
        .controller('CompaniesCtrl', CompaniesCtrl);

    function CompaniesCtrl($log, $state, $timeout, ngDialog, $stateParams, CompaniesService, DriverService, Notification) {
        var vm = this;

        vm.params = {
            page: 1,
            limit: 100,
            city: $stateParams.city,
            state: $stateParams.state,
            zipcode: $stateParams.zipcode,
            metroArea: $stateParams.metroArea,
            empSizeRange: $stateParams.empSizeRange,
            salseVolumeRange: $stateParams.salseVolumeRange
        };
        
        vm.companiesLoaded = false;

        vm.init = function () {
            getCompanies();
            getFavoritesCompanies();
            getStateList();
            getMetroAreaList();
            getEmployeeSizeList();
            getSalesVolumeList();
        };

        vm.getCompanies = getCompanies;
        vm.getCompaniesByParams = getCompaniesByParams;
        vm.addCompanyToFavoriteList = addCompanyToFavoriteList;
        vm.removeFavoriteCompany = removeFavoriteCompany;
        vm.contactCompany = contactCompany;

        vm.companies = [];
        vm.favoriesCompanies = [];
        vm.companiesTotalRecords = 0; 
        vm.totalFavoritesCompanies = 0;

        function getCompanies() {
            function success(response) {
                vm.companies = response.data.data.companyData;
                vm.companiesTotalRecords = response.data.data.totalRecords;
                
                $timeout(function () {
                    vm.companiesLoaded = true;
                }, 5500);

                _compareFavoriteList();
            }

            function failed(response) {
                vm.companiesLoaded = true;

                $log.error(response.data.message);
            }

            CompaniesService
                .companyFilter(vm.params)
                .then(success, failed);
        }

        function getCompaniesByParams() {
            $state.go('main.companies', vm.params);
        }

        function getFavoritesCompanies() {
            function success(response) {
                vm.favoriesCompanies = response.data.companyList;
                _compareFavoriteList();
            }

            function failed(response) {
                $log.error(response.data.message);
            }

            DriverService
                .getFavoritesCompanies({
                    page: 1,
                    limit: 100
                })
                .then(success, failed);
        }
        
        function addCompanyToFavoriteList(company) {
            function success(response) {
                _addFavoriteCompany(company);
                Notification.success(response.data.message);
            }

            function failed(response) {
                Notification.error(response.data.message);
            }

            DriverService
                .addCompanyToDriverFavoriteList(company._id)
                .then(success, failed);
        }

        function removeFavoriteCompany(company) {
            function success(response) {
                _removeFavoriteCompany(company);
                Notification.primary(response.data.message);
            }

            function failed(response) {
                Notification.error(response.data.message);
            }

            DriverService
                .removeFavoriteCompany(company._id)
                .then(success, failed);
        }

        function getSalesVolumeList() {
            function success(response) {
                vm.salesVolumeList = response.data.data;
            }

            function failed(response) {
                $log.error(response.data.message);
            }

            CompaniesService
                .getSalesVolumeList()
                .then(success, failed);
        }

        function getStateList() {
            function success(response) {
                vm.stateList = response.data.data;
            }

            function failed(response) {
                $log.error(response.data.message);
            }

            CompaniesService
                .getStateList()
                .then(success, failed);
        }

        function getMetroAreaList() {
            function success(response) {
                vm.metroAreaList = response.data.data;
            }

            function failed(response) {
                $log.error(response.data.message);
            }

            CompaniesService
                .getMetroAreaList()
                .then(success, failed);
        }

        function getEmployeeSizeList() {
            function success(response) {
                vm.employeeSizeList = response.data.data;
            }

            function failed(response) {
                $log.error(response.data.message);
            }

            CompaniesService
                .getEmployeeSizeList()
                .then(success, failed);
        }

        function _compareFavoriteList() {
            vm.totalFavoritesCompanies = 0;

            vm.companies.forEach(function (company) {
                company.favorite = vm.favoriesCompanies.find(function(favoriteCompany) {
                    return favoriteCompany._id === company._id;
                });

                if (company.favorite)
                    vm.totalFavoritesCompanies++;
            });
        }

        function _addFavoriteCompany(company) {
            vm.favoriesCompanies.push(company);
            _compareFavoriteList();
        }

        function _removeFavoriteCompany(company) {
            var index = vm.favoriesCompanies.findIndex(function (favoriteCompany) {
                return favoriteCompany._id === company._id;
            });

            vm.favoriesCompanies.splice(index, 1);
            _compareFavoriteList();
        }

        function contactCompany(company) {
            var options = {
                templateUrl: '../views/companies/companies.contact.html',
                showClose: false,
                appendClassName: 'ngdialog-default',
                controller: 'CompaniesContactCtrl as vm',
                data: {
                    company: company
                }
            };

            ngDialog.open(options);
        }

    }

})();
 