(function () {
    'use strict'; 

    angular
        .module('main.companies', [])
        .config(config);

    config.$inject = ['$stateProvider', '$urlRouterProvider', '$httpProvider'];
    function config($stateProvider, $urlRouterProvider, $httpProvider) {
        
        $stateProvider
            .state('main.companies', {
                url: 'companies?city&zipcode&metroArea&empSizeRange&salseVolumeRange&state',
                views: {
                    '@main': {
                        templateUrl: '../views/companies/companies.html',
                        controller: 'CompaniesCtrl as vm'
                    },
                    'loading@main.companies': {
                        templateUrl: '../views/loading/loading.html',
                        controller: 'LoadingCtrl as vm'
                    }
                },
                data: {
                    is_granted: ['ROLE_DRIVER']
                }
            });
        
    }

})();
