(function () {
    'use strict';

    angular
        .module('main')
        .service('CompaniesService', function ($http,
                                                    $q,
                                                    $rootScope,
                                                    UPLOAD_CSV_URL,
                                                    COMPANY_FILTER_URL,
                                                    COMPANY_STATE_LIST_URL,
                                                    COMPANY_METRO_AREA_LIST_URL,
                                                    COMPANY_IS_PARTNER_TOGGLE_URL, 
                                                    COMPANY_EMPLOYEE_SIZE_LIST_URL,
                                                    COMPANY_SALES_VOLUME_LIST_URL,
                                                    COMPANY_PARTNER_LIST_URL) {

            this.companyFilter = function (params) {
                return $http.get(COMPANY_FILTER_URL, {
                    params: params
                });
            };

            this.getStateList = function () {
                return $http.get(COMPANY_STATE_LIST_URL);
            };

            this.getMetroAreaList = function () {
                return $http.get(COMPANY_METRO_AREA_LIST_URL);
            };

            this.getEmployeeSizeList = function () {
                return $http.get(COMPANY_EMPLOYEE_SIZE_LIST_URL);
            };

            this.getSalesVolumeList = function () {
                return $http.get(COMPANY_SALES_VOLUME_LIST_URL);
            };

            this.toggleIsPartner = function (id) {
                return $http.get(COMPANY_IS_PARTNER_TOGGLE_URL + '/' + id);
            };

            this.getPartnerCompanies = function () {
                return $http.get(COMPANY_PARTNER_LIST_URL);
            };

            this.uploadCSV = function (file) {
                var fd = new FormData();
                fd.append('file', file);

                var defer = $q.defer();

                var xhttp = new XMLHttpRequest();

                xhttp.upload.addEventListener("progress",function (e) {
                    defer.notify(parseInt(e.loaded * 100 / e.total));
                });
                xhttp.upload.addEventListener("error",function (e) {
                    defer.reject(e);
                });

                xhttp.onreadystatechange = function() {
                    if (xhttp.readyState === 4) {
                        defer.resolve(JSON.parse(xhttp.response)); //Outputs a DOMString by default
                    }
                };

                xhttp.open("post", UPLOAD_CSV_URL, true);
                xhttp.setRequestHeader('Authorization', $rootScope.globals.currentUser.token);

                xhttp.send(fd);

                return defer.promise;
            }

        });  
})();  