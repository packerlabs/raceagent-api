(function () {
    'use strict';

    angular
        .module('main')
        .controller('AdminCtrl', AdminCtrl);

    function AdminCtrl(AuthService, Notification, crAcl, $state) {
        var vm = this;

        vm.user = AuthService.getCurrentUser();
    }

})();
 