(function () {
    'use strict';

    angular
        .module('main')
        .controller('AdminPlansCtrl', AdminPlansCtrl);

    function AdminPlansCtrl(AdminService, Notification, $log) {
        var vm = this;

        getPlans();
        
        function getPlans() {
            function success(response) {
                $log.info(response.data);
            }

            function failed(response) {
                $log.error(response.data.message);
            }

            AdminService
                .getPlanList()
                .then(success, failed);
        }
        
    }

})();
 