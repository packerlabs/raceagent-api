(function () {
    'use strict';

    angular
        .module('admin.plans', []) 
        .config(config);

    config.$inject = ['$stateProvider'];
    function config($stateProvider) {
        
        $stateProvider
            .state('admin.plans', {
                url: '/plans',
                templateUrl: '../views/admin/admin.plans.html',
                controller: 'AdminPlansCtrl as vm',
                data: {
                    is_granted: ['ROLE_ADMIN']
                }
            });
        
    }
 
})();
 