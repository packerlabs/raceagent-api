(function () {
    'use strict';

    angular
        .module('main')
        .service('AdminService', function ($http, COMPANY_FILTER_URL, PLAN_LIST_URL) {
            
            this.getPlanList = function () {
                return $http.get(PLAN_LIST_URL);
            };

        });  
})();  