(function () {
    'use strict';

    angular
        .module('main')
        .controller('AdminSponsorsAddCtrl', AdminSponsorsAddCtrl);

    function AdminSponsorsAddCtrl(SponsorsService,CompaniesService ,Notification, $state) {
        var vm = this;

        vm.sponsor = {};
        vm.partnerCompanies = {};
        vm.creatSponsorship = creatSponsorship;
        
        getPartnerCompanies(); /*calling partner companies for dropdown list*/

        function creatSponsorship(){
            
            function success(response) {
                Notification.success(response.data.message);
                vm.sponsor = {};
            }

            function failed(response) {
                Notification.error(response.data.message);
            }

            SponsorsService
                .createSponsorship(vm.sponsor)
                .then(success, failed);
        }

        function getPartnerCompanies(){
            
            function success(response) {
                vm.partnerCompanies = response.data.data;
            }

            function failed(response) {
                // Notification.error(response.data.message);
            }

            CompaniesService
                .getPartnerCompanies()
                .then(success, failed);   
        }           

    }

})();
 