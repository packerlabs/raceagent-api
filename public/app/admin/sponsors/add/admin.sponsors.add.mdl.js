(function () {
    'use strict';

    angular
        .module('admin.sponsors.add', []) 
        .config(config);

    config.$inject = ['$stateProvider'];
    function config($stateProvider) {
        
        $stateProvider
            .state('admin.sponsors.add', {
                url: '/add',
                views: {
                    '@admin': {
                        templateUrl: '../views/admin/admin.sponsors.add.html',
                        controller: 'AdminSponsorsAddCtrl as vm'
                    }
                },
                data: {
                    is_granted: ['ROLE_ADMIN']
                }
            });
        
    }

})();
 