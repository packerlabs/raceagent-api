(function () {
    'use strict';

    angular
        .module('admin.sponsors', [
            'admin.sponsors.add'
        ]) 
        .config(config);

    config.$inject = ['$stateProvider'];
    function config($stateProvider) {
        
        $stateProvider
            .state('admin.sponsors', {
                url: '/sponsors',
                templateUrl: '../views/admin/admin.sponsors.html',
                controller: 'AdminSponsorCtrl as vm',
                data: {
                    is_granted: ['ROLE_ADMIN']
                }
            });
        
    }
 
})();
 