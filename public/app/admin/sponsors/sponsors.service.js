(function () {
    'use strict';

    angular
        .module('main')
        .service('SponsorsService', function ($http,
                                                    $q,
                                                    $rootScope,
                                                    SPONSOR_URL
                                                    ) {

            this.createSponsorship = function (sponsor) {
                return $http.post(SPONSOR_URL, sponsor);
            };

            this.getSponsors = function (params) {
                return $http.get(SPONSOR_URL , {params: params});
            };

        });  
})();  