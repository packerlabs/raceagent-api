(function () {
    'use strict';

    angular
        .module('admin.mailbox', [
            'admin.mailbox.compose'
        ])
        .config(config);
 
    config.$inject = ['$stateProvider'];
    function config($stateProvider) {
        
        $stateProvider
            .state('admin.mailbox', {
                url: '/mailbox',
                templateUrl: '../views/admin/mailbox/admin.mailbox.html',
                controller: 'AdminMailboxCtrl as vm',
                data: {
                    is_granted: ['ROLE_ADMIN']
                }
            });
        
    }
 
})();
 