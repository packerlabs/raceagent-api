(function () {
    'use strict';

    angular
        .module('admin.mailbox.compose', [])
        .config(config);

    config.$inject = ['$stateProvider'];
    function config($stateProvider) {
        
        $stateProvider
            .state('admin.mailbox.compose', {
                url: '/compose',
                views: {
                    '@admin': {
                        templateUrl: '../views/admin/mailbox/admin.mailbox.compose.html',
                        controller: 'AdminMailboxComposeCtrl as vm'
                    }
                },
                data: {
                    is_granted: ['ROLE_ADMIN']
                }
            });
        
    }
 
})();
 