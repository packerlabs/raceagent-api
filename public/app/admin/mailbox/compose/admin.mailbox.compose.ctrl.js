(function () {
    'use strict';

    angular
        .module('main')
        .controller('AdminMailboxComposeCtrl', AdminMailboxComposeCtrl);

    function AdminMailboxComposeCtrl(DriverService, MailboxService, Notification, $rootScope) {
        var vm = this;

        vm.drivers = [];
        vm.driversForSend = [];
        vm.message = {};

        vm.getDrivers = getDrivers;
        vm.send = send;
        function getDrivers() {
            function success(response) {
                vm.drivers = response.data.data;        
                $('.scrollbar-dynamic').scrollbar({
                    scrolly: false
                });
            }

            function failed(response) {
                Notification.error(response.data.message);
            }

            DriverService
                .getAll()
                .then(success, failed);
        }
        
        function send() {
            vm.driversForSend.forEach(sendMessage);
            console.log(vm.driversForSend);
        }
        
        function sendMessage(driver) {
            function success(response) {
                Notification.success(response.data.message);
            }

            function failed(response) {
                if (response.data.code)
                    Notification.error(response.data.errmsg);
                else
                    Notification.error(response.data.message);
            }

            vm.message.senderMailId = $rootScope.globals.currentUser.email;
            vm.message.receiverMailId = driver.email;

            MailboxService
                .sendMessagesToUser(vm.message)
                .then(success, failed);
        }

    }

})();
 