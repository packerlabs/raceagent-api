(function () {
    'use strict';

    angular
        .module('main')
        .controller('AdminMailboxCtrl', AdminMailboxCtrl);
 
    function AdminMailboxCtrl(MailboxService, $scope, $state, $rootScope, $log) {
        var vm = this;

        init();

        vm.getAllMessages = getAllMessages;
        vm.openMessage = openMessage;
        vm.getMessages = getMessages;

        vm.messages = [];
        vm.tab = 'all';
        vm.counts = {}; 
        
        $scope.$state = $state;
        
        function init() {
            getAllMessages();
            // getMessages('sentbox');
            // getMessages('inbox');
        }

        function getAllMessages() {
            function success(response) {
                vm.messages = response.data.messages;
                vm.tab = 'all';
                vm.counts.all = response.data.messages.length;
                vm.message = null;
                $state.go('admin.mailbox.inbox');
            }

            function failed(response) {
                $log.error(response.data.message);
            }

            MailboxService
                .getAllMessages()
                .then(success, failed);
        }

        function getMessages(tab) {
            function success(response) {
                vm.messages = response.data[tab];
                vm.tab = tab;
                vm.counts[tab] = response.data[tab].length;
                vm.message = null;
                $state.go('admin.mailbox.inbox');
            }

            function failed(response) {
                $log.error(response.data.message);
            }

            MailboxService
                .getMessages($rootScope.globals.currentUser.email)
                .then(success, failed);
        }

        function openMessage($index) {
            vm.message = vm.messages[$index];
        }

    }

})();
 