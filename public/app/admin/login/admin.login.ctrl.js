(function () {
    'use strict';

    angular
        .module('main')
        .controller('AdminLoginCtrl', AdminLoginCtrl);

    function AdminLoginCtrl(AuthService, Notification, crAcl, $state) {
        var vm = this;

        vm.credentials = {};
        vm.loginForm = null;
        
        vm.login = login;

        function login() {
            function success(response) {
                response.data.data.role = 'ROLE_ADMIN';

                AuthService
                    .setCredentials(response.data.data);

                crAcl
                    .setRole('ROLE_ADMIN');

                $state.go('admin.drivers');
            }

            function failed(response) {
                Notification.error(response.data.message);
            }

            if (vm.loginForm.$valid)
                AuthService
                    .login(vm.credentials)
                    .then(success, failed);
        }

    }

})();
 