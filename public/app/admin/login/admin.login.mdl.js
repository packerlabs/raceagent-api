(function () {
    'use strict';

    angular
        .module('admin.login', []) 
        .config(config);

    config.$inject = ['$stateProvider', '$urlRouterProvider', '$httpProvider'];
    function config($stateProvider, $urlRouterProvider, $httpProvider) {
        
        $stateProvider
            .state('admin.login', {
                url: '/login',
                views: {
                    '@': {
                        templateUrl: '../views/admin/admin.login.html',
                        controller: 'AdminLoginCtrl as vm'
                    }
                },
                onEnter: function (AuthService) {
                    AuthService.clearCredentials();
                }
            }); 
        
    }

})();
 