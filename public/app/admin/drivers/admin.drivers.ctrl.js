(function () {
    'use strict';

    angular
        .module('main')
        .controller('AdminDriversCtrl', AdminDriversCtrl);

    function AdminDriversCtrl(DriverService, Notification) {
        var vm = this;

        vm.drivers = [];

        vm.getDrivers = getDrivers;
        vm.removeDriver = removeDriver;

        function getDrivers() {
            function success(response) {
                vm.drivers = response.data.data;

            }

            function failed(response) {
                Notification.error(response.data.message);
            }

            DriverService
                .getAll()
                .then(success, failed);
        }

        function removeDriver(id) {
            function success(response) {
                getDrivers();
                Notification.success(response.data.message);

            }

            function failed(response) {
                Notification.error(response.data.message);
            }

            DriverService
                .delete(id)
                .then(success, failed);
        }

    }

})();
 