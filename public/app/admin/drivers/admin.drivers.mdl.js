(function () {
    'use strict';

    angular
        .module('admin.drivers', [
            'admin.drivers.add',
            'admin.drivers.profile'
        ]) 
        .config(config);

    config.$inject = ['$stateProvider'];
    function config($stateProvider) {
        
        $stateProvider
            .state('admin.drivers', {
                url: '/drivers',
                templateUrl: '../views/admin/admin.drivers.html',
                controller: 'AdminDriversCtrl as vm',
                data: {
                    is_granted: ['ROLE_ADMIN']
                }
            });
        
    }
 
})();
 