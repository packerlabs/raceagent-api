(function () {
    'use strict';

    angular
        .module('main')
        .controller('AdminDriversProfileCtrl', AdminDriversProfileCtrl);

    function AdminDriversProfileCtrl(DriverService, Notification, $stateParams, DRIVER_MEDIA_URL) {
        var vm = this;

        vm.formDriver = null;

        vm.driver = {};

        vm.flow = {};
        vm.flowConfig = {
            target: DRIVER_MEDIA_URL,
            singleFile: true,
            testChunks: false
        };

        vm.getProfile = getProfile;
        vm.update = update;

        function getProfile() {
            function success(response) {
                vm.driver = response.data.data;

                vm.driver.birthdate = new Date(vm.driver.birthdate);
            }

            function failed(response) {
                Notification.error(response.data.message);
            }

            DriverService
                .getProfile($stateParams.id)
                .then(success, failed);
        }

        function update($message) {
            function success() {
                Notification.success('Updated');
                vm.flow.cancel();
            }

            function failed(response) {
                Notification.error(response.data.message);
                vm.flow.cancel();
            }

            if ($message)
                vm.driver.profilePic = JSON.parse($message).fileName;

            if (vm.formDriver.$valid)
                DriverService
                    .update(vm.driver)
                    .then(success, failed);
        }

    }

})();
 