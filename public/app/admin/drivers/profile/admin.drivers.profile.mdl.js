(function () {
    'use strict';
 
    angular
        .module('admin.drivers.profile', [
            'admin.drivers.profile.tabs'
        ]) 
        .config(config);

    config.$inject = ['$stateProvider'];
    function config($stateProvider) {
        
        $stateProvider
            .state('admin.drivers.profile', {
                url: '/profile/:id',
                views: {
                    '@admin': {
                        templateUrl: '../views/admin/profile/admin.drivers.profile.html',
                        controller: 'AdminDriversProfileCtrl as vm'
                    }
                },
                abstract: true,
                data: {
                    is_granted: ['ROLE_ADMIN']
                }
            });
        
    }

})();
 