(function () {
    'use strict';
 
    angular
        .module('admin.drivers.profile.tabs.general', []) 
        .config(config);

    config.$inject = ['$stateProvider'];
    function config($stateProvider) {
        
        $stateProvider
            .state('admin.drivers.profile.tabs.general', {
                url: '',
                views: {
                    'tabs@admin.drivers.profile': {
                        templateUrl: '../views/admin/profile/tabs/admin.drivers.profile.tabs.general.html'
                    }
                },
                data: {
                    is_granted: ['ROLE_ADMIN']
                }
            });
        
    }

})();
 