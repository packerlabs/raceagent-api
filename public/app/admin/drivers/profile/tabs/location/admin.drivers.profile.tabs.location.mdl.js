(function () {
    'use strict';
 
    angular
        .module('admin.drivers.profile.tabs.location', []) 
        .config(config);

    config.$inject = ['$stateProvider'];
    function config($stateProvider) {
        
        $stateProvider
            .state('admin.drivers.profile.tabs.location', {
                url: '/location',
                views: {
                    'tabs@admin.drivers.profile': {
                        templateUrl: '../views/admin/profile/tabs/admin.drivers.profile.tabs.location.html'
                    }
                },
                data: {
                    is_granted: ['ROLE_ADMIN']
                }
            });
        
    }

})();
 