(function () {
    'use strict';
 
    angular
        .module('admin.drivers.profile.tabs.about', []) 
        .config(config);

    config.$inject = ['$stateProvider'];
    function config($stateProvider) {
        
        $stateProvider
            .state('admin.drivers.profile.tabs.about', {
                url: '/about',
                views: {
                    'tabs@admin.drivers.profile': {
                        templateUrl: '../views/admin/profile/tabs/admin.drivers.profile.tabs.about.html'
                    }
                },
                data: {
                    is_granted: ['ROLE_ADMIN']
                }
            });
        
    }

})();
 