(function () {
    'use strict';
 
    angular
        .module('admin.drivers.profile.tabs', [
            'admin.drivers.profile.tabs.about',
            'admin.drivers.profile.tabs.location',
            'admin.drivers.profile.tabs.general',
            'admin.drivers.profile.tabs.links'
        ]) 
        .config(config);

    config.$inject = ['$stateProvider'];
    function config($stateProvider) {
        
        $stateProvider
            .state('admin.drivers.profile.tabs', {
                url: '',
                abstract: true,
                data: {
                    is_granted: ['ROLE_ADMIN']
                }
            });
        
    }

})();
 