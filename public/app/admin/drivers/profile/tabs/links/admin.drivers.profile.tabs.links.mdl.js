(function () {
    'use strict';
 
    angular
        .module('admin.drivers.profile.tabs.links', [])
        .config(config);

    config.$inject = ['$stateProvider'];
    function config($stateProvider) {
        
        $stateProvider
            .state('admin.drivers.profile.tabs.links', {
                url: '/links',
                views: {
                    'tabs@admin.drivers.profile': {
                        templateUrl: '../views/admin/profile/tabs/admin.drivers.profile.tabs.links.html'
                    }
                },
                data: {
                    is_granted: ['ROLE_ADMIN']
                }
            });
        
    }

})();
 