(function () {
    'use strict';

    angular
        .module('main')
        .controller('AdminDriversAddCtrl', AdminDriversAddCtrl);

    function AdminDriversAddCtrl(DriverService, Notification, $state) {
        var vm = this;

        vm.driver = {}; 
        vm.generalForm = null;
        
        vm.create = create;

        function create() {
            function success(response) {
                Notification.success(response.data.message);
                $state.go('admin.drivers');
            }

            function failed(response) {
                Notification.error(response.data.message);
            }

            if (vm.generalForm.$valid)
                DriverService
                    .register(vm.driver)
                    .then(success, failed);
        }

    }

})();
 