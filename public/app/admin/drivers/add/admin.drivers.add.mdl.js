(function () {
    'use strict';

    angular
        .module('admin.drivers.add', []) 
        .config(config);

    config.$inject = ['$stateProvider'];
    function config($stateProvider) {
        
        $stateProvider
            .state('admin.drivers.add', {
                url: '/add',
                views: {
                    '@admin': {
                        templateUrl: '../views/admin/admin.drivers.add.html',
                        controller: 'AdminDriversAddCtrl as vm'
                    }
                },
                data: {
                    is_granted: ['ROLE_ADMIN']
                }
            });
        
    }

})();
 