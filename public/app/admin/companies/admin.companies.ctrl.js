(function () {
    'use strict';

    angular
        .module('main')
        .controller('AdminCompaniesCtrl', AdminCompaniesCtrl);
 
    function AdminCompaniesCtrl(UPLOAD_CSV_URL, DriverService, CompaniesService, Notification, $rootScope, $log) {
        var vm = this;

        init();

        vm.uploadCompleted = uploadCompleted;
        vm.uploadError = uploadError;
        vm.getCompanies = getCompanies;
        vm.uploadCSV = uploadCSV;
        vm.isPartner = isPartner;
        
        vm.flow = {};
        vm.flowConfig = {
            target: UPLOAD_CSV_URL,
            singleFile: true,
            testChunks: false,
            headers: {
                'Authorization' : $rootScope.globals.currentUser.token
            }
        };

        vm.params = {
            page: 1,
            limit: 10,
            city: null,
            zipcode: null,
            metroArea: null,
            empSizeRange: null,
            salseVolumeRange: null
        };

        vm.paramsDriversFavoriteCompany = {
            page: 1,
            limit: 10,
            search: '596e6112523b70105ba9e8cd'
        };

        vm.companies = [];
        vm.columns = [];

        function init() {
            getStateList();
            getMetroAreaList();
            getEmployeeSizeList();
            getSalesVolumeList();
        }

        function getCompanies() {
            function success(response) {
                vm.companies = response.data.data.companyData;
                vm.totalRecords = response.data.data.totalRecords;
                vm.columns = []; //fixed columns repeat issue  
                if (vm.companies.length) 
                    for (var key in vm.companies[0]) 
                        if (key !== '_id')
                            vm.columns.push(key);
            }
 
            function failed(response) {
                $log.error(response.data.message);
            }

            CompaniesService
                .companyFilter(vm.params)
                .then(success, failed);
        }

        function uploadCSV() {
            CompaniesService
                .uploadCSV(vm.flow.files[0].file)
                .then(function(response){

                    uploadCompleted(response.message);

                }, function(response){
                    uploadError(response.message);
                }, function(progress){
                    vm.uploadProgress = progress;
                });
        }
        
        function uploadCompleted(message) {
            Notification.success(message);
            vm.flow.cancel();
            getCompanies();
            $log.info(message);
            vm.uploadProgress = 0;
        }

        function uploadError(message) {
            Notification.error(message);
            $log.error(message);
        }

        function getSalesVolumeList() {
            function success(response) {
                vm.salesVolumeList = response.data.data;
            }

            function failed(response) {
                $log.error(response.data.message);
            }

            CompaniesService
                .getSalesVolumeList()
                .then(success, failed);
        }

        function getStateList() {
            function success(response) {
                vm.stateList = response.data.data;
            }

            function failed(response) {
                $log.error(response.data.message);
            }

            CompaniesService
                .getStateList()
                .then(success, failed);
        }

        function getMetroAreaList() {
            function success(response) {
                vm.metroAreaList = response.data.data;
            }

            function failed(response) {
                $log.error(response.data.message);
            }

            CompaniesService
                .getMetroAreaList()
                .then(success, failed);
        }

        function getEmployeeSizeList() {
            function success(response) {
                vm.employeeSizeList = response.data.data;
            }

            function failed(response) {
                $log.error(response.data.message);
            }

            CompaniesService
                .getEmployeeSizeList()
                .then(success, failed);
        }

        function isPartner(rowIndex,colIndex) {
            
            function success(response) {
                getCompanies();
                Notification.success(response.data.message);
            }

            function failed(response) {
                $log.error(response.data.message);
            }

            CompaniesService.
                toggleIsPartner(vm.companies[rowIndex]._id)
                .then(success,failed);
            console.log('isPartner is clicked.',vm.companies[rowIndex]._id);
        }

    }

})();
 