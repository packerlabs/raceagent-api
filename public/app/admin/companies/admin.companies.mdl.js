(function () {
    'use strict';

    angular
        .module('admin.companies', []) 
        .config(config);

    config.$inject = ['$stateProvider'];
    function config($stateProvider) {
        
        $stateProvider
            .state('admin.companies', {
                url: '/companies',
                templateUrl: '../views/admin/admin.companies.html',
                controller: 'AdminCompaniesCtrl as vm',
                data: {
                    is_granted: ['ROLE_ADMIN']
                }
            });
        
    }
 
})();
 