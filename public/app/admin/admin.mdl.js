(function () {
    'use strict';
 
    angular
        .module('admin', [
            'admin.drivers',
            'admin.companies',
            'admin.plans',
            'admin.login',
            'admin.mailbox',
            'admin.sponsors'
        ]) 
        .config(config);

    config.$inject = ['$stateProvider', '$urlRouterProvider', '$httpProvider'];
    function config($stateProvider, $urlRouterProvider, $httpProvider) {
        
        $stateProvider
            .state('admin', {
                url: '/admin',
                abstract: true,
                templateUrl: '../views/admin/admin.html',
                controller: 'AdminCtrl as admin'
            });
        
    }

})();
 