(function () {
    'use strict';

    angular
        .module('main.favorites', [])
        .config(config);

    config.$inject = ['$stateProvider', '$urlRouterProvider', '$httpProvider'];
    function config($stateProvider, $urlRouterProvider, $httpProvider) {
        
        $stateProvider
            .state('main.favorites', {
                url: 'favorites',
                templateUrl: '../views/favorites/favorites.html',
                controller: 'FavoritesCtrl as vm',
                data: {
                    is_granted: ['ROLE_DRIVER']
                }
            });
        
    }

})();
 