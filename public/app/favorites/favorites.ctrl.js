(function () {
    'use strict';

    angular
        .module('main')
        .controller('FavoritesCtrl', FavoritesCtrl);

    function FavoritesCtrl($log, DriverService, ngDialog, Notification) {
        var vm = this;

        vm.init = function () {
            getFavoritesCompanies();
        };

        vm.getFavoritesCompanies = getFavoritesCompanies;
        vm.removeFavoriteCompany = removeFavoriteCompany;
        vm.contactCompany = contactCompany;

        vm.params = {
            page: 1,
            limit: 10,
            search: null
        };

        function getFavoritesCompanies() {
            function success(response) {
                vm.companies = response.data.companyList;
                vm.companiesTotalRecords = response.data.totalRecords;
            }

            function failed(response) {
                $log.error(response.data.message);
            }

            DriverService
                .getFavoritesCompanies(vm.params)
                .then(success, failed);
        }

        function removeFavoriteCompany(companyId) {
            function success(response) {
                getFavoritesCompanies();
                Notification.primary(response.data.message);
            }

            function failed(response) {
                Notification.error(response.data.message);
            }

            DriverService
                .removeFavoriteCompany(companyId)
                .then(success, failed);
        }

        function contactCompany(company) {
            var options = {
                templateUrl: '../views/companies/companies.contact.html',
                showClose: false,
                appendClassName: 'ngdialog-default',
                controller: 'CompaniesContactCtrl as vm',
                data: {
                    company: company
                }
            };

            ngDialog.open(options);
        }

    }

})();
 