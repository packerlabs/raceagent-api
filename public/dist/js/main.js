angular.module("config", [])
    .constant("DRIVER_URL", "/api/driver")
    .constant("DRIVER_LIST_URL", "/api/driversList")
    .constant("DRIVER_CARD_DETAIL", "/api/driver/account/card")
    .constant("DRIVER_MEDIA_URL", "/api/driver/profilePicture")
    .constant("DRIVER_PLAN_URL", "/api/driver/plan/subscription")
    .constant("DRIVER_COMPANY_FAVORITE_URL", "/api/driver/company/favorite")
    .constant("COMPANY_FILTER_URL", "/api/company/filter")
    .constant("COMPANY_STATE_LIST_URL", "/api/company/stateList")
    .constant("COMPANY_IS_PARTNER_TOGGLE_URL", "/api/company/isPartner")
    .constant("COMPANY_PARTNER_LIST_URL", "/api/company/partnerList")
    .constant("COMPANY_METRO_AREA_LIST_URL", "/api/company/metroAreaList")
    .constant("COMPANY_EMPLOYEE_SIZE_LIST_URL", "/api/company/employeeSizeList")
    .constant("COMPANY_SALES_VOLUME_LIST_URL", "/api/company/salesVolumeList")
    .constant("PLAN_LIST_URL", "/api/plan")
    .constant("PLAN_SUBSCRIBE_URL", "/api/plan/subscription")
    .constant("PLAN_CHANGE_URL", "/api/plan/change")
    .constant("MESSAGE_URL", "/api/messages")
    .constant("UPLOAD_CSV_URL", "/api/uploadCsv")
    .constant("UPLOAD_PDF_URL", "/api/uploadPdf")
    .constant("DRIVER_PAYMENT_DETAIL_UPDATE", "/api/driver/account/card")
    .constant("SPONSOR_URL", "/api/sponsors");
  
    


(function () {
    'use strict';

    angular
        .module('main', [
            'ui.router',
            'ui.bootstrap',
            'ngMask',
            'ngCookies',
            'ngAnimate',
            'ngDialog',
            'cr.acl',
            'flow',
            'checklist-model',
            'ui-notification',
            'uiSwitch',
            'nvd3', 
            'ngFlash',
            'textAngular',
            'ngSanitize',
            'ngImgCrop',
            'localytics.directives',
            'jQueryScrollbar',

            'main.favorites',
            'main.companies',
            'main.loading',
            'main.driver',
            'main.search',
            'main.settings',
            'main.membership',
            'main.mailbox',
            'main.welcome',
            'main.partners',
            'main.sponsorships',

            'main.auth',

            'admin',
            'config'
        ])
        .config(config)
        .run(run);

    config.$inject = ['$stateProvider', '$urlRouterProvider', 'NotificationProvider'];
    function config($stateProvider, $urlRouterProvider, NotificationProvider) {

        $urlRouterProvider.otherwise(function ($injector) {
            var $state = $injector.get('$state');
            var $rootScope = $injector.get('$rootScope');
            var crAcl = $injector.get('crAcl');

            switch (crAcl.getRole()) {
                case 'ROLE_GUEST' : $state.go('main.auth.signup');
                    break;
                case 'ROLE_DRIVER' : $state.go('main.search');
                    break;
                case 'ROLE_ADMIN' : $state.go('admin.drivers');
                    break;
            }
            
        });
 
        $stateProvider 
            .state('main', {
                url: '/',
                templateUrl: '../views/main.html',
                abstract: true
            })
            .state('main.sitemap', {
                url: 'sitemap',
                templateUrl: '../views/sitemap.html'
            });

        NotificationProvider.setOptions({
            replaceMessage: true
        });
    }

    run.$inject = ['$rootScope', '$cookieStore', '$http', 'crAcl', 'MemberShipService', '$state'];
    function run($rootScope, $cookieStore, $http, crAcl, MemberShipService, $state) {

        // keep user logged in after page refresh
        $rootScope.globals = $cookieStore.get('globals') || {};

        crAcl
            .setInheritanceRoles({
                'ROLE_DRIVER': ['ROLE_DRIVER', 'ROLE_GUEST'],
                'ROLE_ADMIN': ['ROLE_DRIVER', 'ROLE_ADMIN', 'ROLE_GUEST'],
                'ROLE_GUEST': ['ROLE_GUEST']
            });

        crAcl
            .setRedirect('main.auth.signup');

        if ($rootScope.globals.currentUser) {
            $http.defaults.headers.common['Authorization'] = $rootScope.globals.currentUser.token;
            crAcl.setRole($rootScope.globals.currentUser.role);
        }
        else {
            crAcl.setRole(); 
        }

        function getSubscriptionByDriverId() {
            function success(response) {
                if (!response.data.data.length)
                    $state.go('main.membership');
            }

            if (crAcl.getRole() === 'ROLE_DRIVER')
                MemberShipService
                    .getSubscriptionByDriverId($rootScope.globals.currentUser.driver._id)
                    .then(success);
        }

        $rootScope.$on('$stateChangeStart', getSubscriptionByDriverId);

    }

})();
 
(function () {
    'use strict';

    angular
        .module('main')
        .controller('AdminCtrl', AdminCtrl);

    function AdminCtrl(AuthService, Notification, crAcl, $state) {
        var vm = this;

        vm.user = AuthService.getCurrentUser();
    }

})();
 
(function () {
    'use strict';
 
    angular
        .module('admin', [
            'admin.drivers',
            'admin.companies',
            'admin.plans',
            'admin.login',
            'admin.mailbox',
            'admin.sponsors'
        ]) 
        .config(config);

    config.$inject = ['$stateProvider', '$urlRouterProvider', '$httpProvider'];
    function config($stateProvider, $urlRouterProvider, $httpProvider) {
        
        $stateProvider
            .state('admin', {
                url: '/admin',
                abstract: true,
                templateUrl: '../views/admin/admin.html',
                controller: 'AdminCtrl as admin'
            });
        
    }

})();
 
(function () {
    'use strict';

    angular
        .module('main')
        .service('AdminService', function ($http, COMPANY_FILTER_URL, PLAN_LIST_URL) {
            
            this.getPlanList = function () {
                return $http.get(PLAN_LIST_URL);
            };

        });  
})();  
(function () {
    'use strict';

    angular
        .module('main.auth', [
            'main.auth.signup',
            'main.auth.signin',
            'main.auth.forgotPassword',
            'main.auth.resetPassword',
            'main.auth.verifyAccount'
        ])
        .config(config);

    config.$inject = ['$stateProvider', '$urlRouterProvider', '$httpProvider'];
    function config($stateProvider, $urlRouterProvider, $httpProvider) {

        $stateProvider
            .state('main.auth', {
                url: '',
                abstract: true
            });

    }

})();

(function () {
    'use strict';

    angular
        .module('main')
        .service('AuthService', function ($http, $cookieStore, $rootScope, $q, crAcl) {

            var urlBase = '/api/';

            var authService = this;
            
            authService.login = function (credentials) {
                return $http.post(urlBase + 'admin/login', credentials);
            };
            
            authService.setCredentials = function (user) {
                $rootScope.globals = {
                    currentUser: user
                };

                $http.defaults.headers.common['Authorization'] = user.token;
                $cookieStore.put('globals', $rootScope.globals);
            };

            authService.clearCredentials = function () {
                var deferred = $q.defer();
                $cookieStore.remove('globals');

                if (!$cookieStore.get('globals')) {
                    $rootScope.globals = {};
                    $http.defaults.headers.common.Authorization = '';
                    crAcl.setRole();
                    deferred.resolve('Credentials clear success');
                } else {
                    deferred.reject('Can\'t clear credentials');
                }

                return deferred.promise;
            };

            authService.getCurrentUser = function () {
                return $rootScope.globals.currentUser;
            };
            
            authService.verifyUserAccount = function (driverId) {
                return $http.get(urlBase + 'driver/verifyUserAccount/' + driverId);
            }

        });  
})();  
(function () {
    'use strict';

    angular
        .module('main')
        .controller('CompaniesCtrl', CompaniesCtrl);

    function CompaniesCtrl($log, $state, $timeout, ngDialog, $stateParams, CompaniesService, DriverService, Notification) {
        var vm = this;

        vm.params = {
            page: 1,
            limit: 100,
            city: $stateParams.city,
            state: $stateParams.state,
            zipcode: $stateParams.zipcode,
            metroArea: $stateParams.metroArea,
            empSizeRange: $stateParams.empSizeRange,
            salseVolumeRange: $stateParams.salseVolumeRange
        };
        
        vm.companiesLoaded = false;

        vm.init = function () {
            getCompanies();
            getFavoritesCompanies();
            getStateList();
            getMetroAreaList();
            getEmployeeSizeList();
            getSalesVolumeList();
        };

        vm.getCompanies = getCompanies;
        vm.getCompaniesByParams = getCompaniesByParams;
        vm.addCompanyToFavoriteList = addCompanyToFavoriteList;
        vm.removeFavoriteCompany = removeFavoriteCompany;
        vm.contactCompany = contactCompany;

        vm.companies = [];
        vm.favoriesCompanies = [];
        vm.companiesTotalRecords = 0; 
        vm.totalFavoritesCompanies = 0;

        function getCompanies() {
            function success(response) {
                vm.companies = response.data.data.companyData;
                vm.companiesTotalRecords = response.data.data.totalRecords;
                
                $timeout(function () {
                    vm.companiesLoaded = true;
                }, 5500);

                _compareFavoriteList();
            }

            function failed(response) {
                vm.companiesLoaded = true;

                $log.error(response.data.message);
            }

            CompaniesService
                .companyFilter(vm.params)
                .then(success, failed);
        }

        function getCompaniesByParams() {
            $state.go('main.companies', vm.params);
        }

        function getFavoritesCompanies() {
            function success(response) {
                vm.favoriesCompanies = response.data.companyList;
                _compareFavoriteList();
            }

            function failed(response) {
                $log.error(response.data.message);
            }

            DriverService
                .getFavoritesCompanies({
                    page: 1,
                    limit: 100
                })
                .then(success, failed);
        }
        
        function addCompanyToFavoriteList(company) {
            function success(response) {
                _addFavoriteCompany(company);
                Notification.success(response.data.message);
            }

            function failed(response) {
                Notification.error(response.data.message);
            }

            DriverService
                .addCompanyToDriverFavoriteList(company._id)
                .then(success, failed);
        }

        function removeFavoriteCompany(company) {
            function success(response) {
                _removeFavoriteCompany(company);
                Notification.primary(response.data.message);
            }

            function failed(response) {
                Notification.error(response.data.message);
            }

            DriverService
                .removeFavoriteCompany(company._id)
                .then(success, failed);
        }

        function getSalesVolumeList() {
            function success(response) {
                vm.salesVolumeList = response.data.data;
            }

            function failed(response) {
                $log.error(response.data.message);
            }

            CompaniesService
                .getSalesVolumeList()
                .then(success, failed);
        }

        function getStateList() {
            function success(response) {
                vm.stateList = response.data.data;
            }

            function failed(response) {
                $log.error(response.data.message);
            }

            CompaniesService
                .getStateList()
                .then(success, failed);
        }

        function getMetroAreaList() {
            function success(response) {
                vm.metroAreaList = response.data.data;
            }

            function failed(response) {
                $log.error(response.data.message);
            }

            CompaniesService
                .getMetroAreaList()
                .then(success, failed);
        }

        function getEmployeeSizeList() {
            function success(response) {
                vm.employeeSizeList = response.data.data;
            }

            function failed(response) {
                $log.error(response.data.message);
            }

            CompaniesService
                .getEmployeeSizeList()
                .then(success, failed);
        }

        function _compareFavoriteList() {
            vm.totalFavoritesCompanies = 0;

            vm.companies.forEach(function (company) {
                company.favorite = vm.favoriesCompanies.find(function(favoriteCompany) {
                    return favoriteCompany._id === company._id;
                });

                if (company.favorite)
                    vm.totalFavoritesCompanies++;
            });
        }

        function _addFavoriteCompany(company) {
            vm.favoriesCompanies.push(company);
            _compareFavoriteList();
        }

        function _removeFavoriteCompany(company) {
            var index = vm.favoriesCompanies.findIndex(function (favoriteCompany) {
                return favoriteCompany._id === company._id;
            });

            vm.favoriesCompanies.splice(index, 1);
            _compareFavoriteList();
        }

        function contactCompany(company) {
            var options = {
                templateUrl: '../views/companies/companies.contact.html',
                showClose: false,
                appendClassName: 'ngdialog-default',
                controller: 'CompaniesContactCtrl as vm',
                data: {
                    company: company
                }
            };

            ngDialog.open(options);
        }

    }

})();
 
(function () {
    'use strict'; 

    angular
        .module('main.companies', [])
        .config(config);

    config.$inject = ['$stateProvider', '$urlRouterProvider', '$httpProvider'];
    function config($stateProvider, $urlRouterProvider, $httpProvider) {
        
        $stateProvider
            .state('main.companies', {
                url: 'companies?city&zipcode&metroArea&empSizeRange&salseVolumeRange&state',
                views: {
                    '@main': {
                        templateUrl: '../views/companies/companies.html',
                        controller: 'CompaniesCtrl as vm'
                    },
                    'loading@main.companies': {
                        templateUrl: '../views/loading/loading.html',
                        controller: 'LoadingCtrl as vm'
                    }
                },
                data: {
                    is_granted: ['ROLE_DRIVER']
                }
            });
        
    }

})();

(function () {
    'use strict';

    angular
        .module('main')
        .service('CompaniesService', function ($http,
                                                    $q,
                                                    $rootScope,
                                                    UPLOAD_CSV_URL,
                                                    COMPANY_FILTER_URL,
                                                    COMPANY_STATE_LIST_URL,
                                                    COMPANY_METRO_AREA_LIST_URL,
                                                    COMPANY_IS_PARTNER_TOGGLE_URL, 
                                                    COMPANY_EMPLOYEE_SIZE_LIST_URL,
                                                    COMPANY_SALES_VOLUME_LIST_URL,
                                                    COMPANY_PARTNER_LIST_URL) {

            this.companyFilter = function (params) {
                return $http.get(COMPANY_FILTER_URL, {
                    params: params
                });
            };

            this.getStateList = function () {
                return $http.get(COMPANY_STATE_LIST_URL);
            };

            this.getMetroAreaList = function () {
                return $http.get(COMPANY_METRO_AREA_LIST_URL);
            };

            this.getEmployeeSizeList = function () {
                return $http.get(COMPANY_EMPLOYEE_SIZE_LIST_URL);
            };

            this.getSalesVolumeList = function () {
                return $http.get(COMPANY_SALES_VOLUME_LIST_URL);
            };

            this.toggleIsPartner = function (id) {
                return $http.get(COMPANY_IS_PARTNER_TOGGLE_URL + '/' + id);
            };

            this.getPartnerCompanies = function () {
                return $http.get(COMPANY_PARTNER_LIST_URL);
            };

            this.uploadCSV = function (file) {
                var fd = new FormData();
                fd.append('file', file);

                var defer = $q.defer();

                var xhttp = new XMLHttpRequest();

                xhttp.upload.addEventListener("progress",function (e) {
                    defer.notify(parseInt(e.loaded * 100 / e.total));
                });
                xhttp.upload.addEventListener("error",function (e) {
                    defer.reject(e);
                });

                xhttp.onreadystatechange = function() {
                    if (xhttp.readyState === 4) {
                        defer.resolve(JSON.parse(xhttp.response)); //Outputs a DOMString by default
                    }
                };

                xhttp.open("post", UPLOAD_CSV_URL, true);
                xhttp.setRequestHeader('Authorization', $rootScope.globals.currentUser.token);

                xhttp.send(fd);

                return defer.promise;
            }

        });  
})();  
(function () {
    'use strict';

    angular
        .module('main')
        .directive('onOutsideElementClick', onOutsideElementClick);
    
        function onOutsideElementClick($document) {
            return {
                restrict: 'A',
                link: function(scope, element, attrs) {
 
                    element.on('click', function(e) {
                        e.stopPropagation();
                    });

                    var onClick = function() {
                        scope.$apply(function() {
                            scope.$eval(attrs.onOutsideElementClick);
                        });
                    };

                    $document.on('click', onClick);

                    scope.$on('$destroy', function() {
                        $document.off('click', onClick);
                    });
                }
            };
        }
})();
  
(function () {
    'use strict';

    angular
        .module('main.driver', [ 
            'main.driver.account',
            'main.driver.profile'
        ])
        .config(config);

    config.$inject = ['$stateProvider', '$urlRouterProvider', '$httpProvider'];
    function config($stateProvider, $urlRouterProvider, $httpProvider) {
        
        $stateProvider
            .state('main.driver', {
                url: 'driver/',
                abstract: true
            });
        
    }

})();
 
(function () {
    'use strict';

    angular
        .module('main')
        .service('DriverService', function ($http, $q, $rootScope, DRIVER_URL, DRIVER_MEDIA_URL, DRIVER_LIST_URL, DRIVER_COMPANY_FAVORITE_URL,DRIVER_CARD_DETAIL,DRIVER_PAYMENT_DETAIL_UPDATE,DRIVER_PLAN_URL,PLAN_LIST_URL,PLAN_CHANGE_URL) {
            this.getCard = function(){
                return $http.get(DRIVER_CARD_DETAIL);
            };

            this.updatePaymentDetails = function(cardId,data){
                return $http.put(DRIVER_PAYMENT_DETAIL_UPDATE + '/' + cardId, data);
            };
            
            this.getSubscribedPlan = function(){
                return $http.get(DRIVER_PLAN_URL);
            };

            this.getDriverPlanList = function () {
                return $http.get(PLAN_LIST_URL);
            };

            this.changePlan = function(plan){
                return $http.put(PLAN_CHANGE_URL,plan);
            };

            this.register = function (driver) {
                return $http.post(DRIVER_URL, driver);
            };

            this.getProfile = function (id) {
                return $http.get(DRIVER_URL + '/' + id);
            };

            this.getProfileByUsername = function (username) {
                return $http.get(DRIVER_URL + '/username/' + username);
            };

            this.getFavoritesCompanies = function (params) {
                return $http.get(DRIVER_COMPANY_FAVORITE_URL, {
                    params: params
                });
            };

            this.removeFavoriteCompany = function (companyId) {
                return $http.delete(DRIVER_COMPANY_FAVORITE_URL + '/' + companyId);
            };

            this.addCompanyToDriverFavoriteList = function (companyId) {
                return $http.post(DRIVER_COMPANY_FAVORITE_URL, {
                    companyId: companyId
                });
            };

            this.login = function (driver) {
                return $http.post(DRIVER_URL + '/login', driver);
            };

            this.update = function (driver) {
                return $http.put(DRIVER_URL + '/' + driver._id, driver);
            };

            this.getAll = function () {
                return $http.get(DRIVER_LIST_URL);
            };

            this.delete = function (id) {
                return $http.delete(DRIVER_URL + '/' + id);
            };

            this.forgotPassword = function (email) {
                return $http.post(DRIVER_URL + '/forgotPassword', { email: email });
            };

            this.resetPassword = function (data) {
                return $http.post(DRIVER_URL + '/resetPassword', data);
            };

            this.verifyDriverAccount = function (data) {
              return $http.get(DRIVER_URL + '/verifyUserAccount/' + data.token);
            }

            this.uploadAvatar = function (base64) {

                var defer = $q.defer();

                var mimeType = /data\:([^)]+)\;/.exec(base64)[1];
                var fileName = 'avatar.' + (/\/(.*)/.exec(mimeType)[1]);

                convertBase64ToFile(base64, fileName, mimeType)
                    .then(function(file){
                        var fd = new FormData();

                        fd.append('file', file);

                        var xhttp = new XMLHttpRequest();

                        xhttp.upload.addEventListener("progress",function (e) {
                            defer.notify(parseInt(e.loaded * 100 / e.total));
                        });
                        xhttp.upload.addEventListener("error",function (e) {
                            defer.reject(e);
                        });

                        xhttp.onreadystatechange = function() {
                            if (xhttp.readyState === 4) {
                                defer.resolve(JSON.parse(xhttp.response)); //Outputs a DOMString by default
                            }
                        };

                        xhttp.open("post", DRIVER_MEDIA_URL, true);
                        xhttp.setRequestHeader('Authorization', $rootScope.globals.currentUser.token);

                        xhttp.send(fd);
                    });

                return defer.promise;

            };

            function convertBase64ToFile(url, filename, mimeType) {
                return (fetch(url)
                        .then(function(res){return res.arrayBuffer();})
                        .then(function(buf){return new File([buf], filename, {type:mimeType});})
                );
            }

        });
})();

(function () {
    'use strict';

    angular
        .module('main')
        .controller('FavoritesCtrl', FavoritesCtrl);

    function FavoritesCtrl($log, DriverService, ngDialog, Notification) {
        var vm = this;

        vm.init = function () {
            getFavoritesCompanies();
        };

        vm.getFavoritesCompanies = getFavoritesCompanies;
        vm.removeFavoriteCompany = removeFavoriteCompany;
        vm.contactCompany = contactCompany;

        vm.params = {
            page: 1,
            limit: 10,
            search: null
        };

        function getFavoritesCompanies() {
            function success(response) {
                vm.companies = response.data.companyList;
                vm.companiesTotalRecords = response.data.totalRecords;
            }

            function failed(response) {
                $log.error(response.data.message);
            }

            DriverService
                .getFavoritesCompanies(vm.params)
                .then(success, failed);
        }

        function removeFavoriteCompany(companyId) {
            function success(response) {
                getFavoritesCompanies();
                Notification.primary(response.data.message);
            }

            function failed(response) {
                Notification.error(response.data.message);
            }

            DriverService
                .removeFavoriteCompany(companyId)
                .then(success, failed);
        }

        function contactCompany(company) {
            var options = {
                templateUrl: '../views/companies/companies.contact.html',
                showClose: false,
                appendClassName: 'ngdialog-default',
                controller: 'CompaniesContactCtrl as vm',
                data: {
                    company: company
                }
            };

            ngDialog.open(options);
        }

    }

})();
 
(function () {
    'use strict';

    angular
        .module('main.favorites', [])
        .config(config);

    config.$inject = ['$stateProvider', '$urlRouterProvider', '$httpProvider'];
    function config($stateProvider, $urlRouterProvider, $httpProvider) {
        
        $stateProvider
            .state('main.favorites', {
                url: 'favorites',
                templateUrl: '../views/favorites/favorites.html',
                controller: 'FavoritesCtrl as vm',
                data: {
                    is_granted: ['ROLE_DRIVER']
                }
            });
        
    }

})();
 
(function () {
    'use strict';

    angular
        .module('main')
        .controller('LoadingCtrl', LoadingCtrl);

    function LoadingCtrl($interval, $state, $http) {
        var vm = this;
        var i = 0;
        var iSum = 0;

        vm.style = {
            'background-image': null
        };

        vm.steps = [
            {
                name: 'READY',
                img: '../dist/images/ready.png'
            },
            {
                name: 'SET',
                img: '../dist/images/set.png'
            },
            {
                name: 'GO',
                img: '../dist/images/go.png'
            }
        ];

        preloadImages(vm.steps);
        vm.style = {
            'background-image': 'url(' + preloadImages.cache[0].src + ')'
        };

        function preloadImages() {
            if (!preloadImages.cache) {
                preloadImages.cache = [];
            }
            var img;
            vm.steps.forEach(function (item) {
                img = new Image();
                img.src = item.img;
                preloadImages.cache.push(img);
            });
        }

        console.log(preloadImages.cache);
        function loading() {
            if (i < 3 && iSum < 36) {
                vm.step = vm.steps[i];
                vm.style['background-image'] = 'url(' + preloadImages.cache[i].src + ')';
                vm.style.opacity = 1;
                i++;
                iSum++;
            } else if (i > 0) {
                i = 0;
            }
        }

        $interval(loading, 1500);
    }

})();
 
(function () {
    'use strict';

    angular
        .module('main.loading', [])
        .config(config);

    config.$inject = ['$stateProvider', '$urlRouterProvider', '$httpProvider'];
    function config($stateProvider, $urlRouterProvider, $httpProvider) {
        
        $stateProvider
            .state('main.loading', {
                url: 'loading',
                templateUrl: '../views/loading/loading.html',
                controller: 'LoadingCtrl as vm'
            });
        
    }

})();
 
(function () {
    'use strict';

    angular
        .module('main')
        .controller('MailboxCtrl', MailboxCtrl);
 
    function MailboxCtrl(MailboxService, Notification, $rootScope) {
        var vm = this;

        vm.messages = {};
        vm.tab = 'inbox';

        vm.getMessages = getMessages;
        vm.openMessage = openMessage;
        vm.setTab = setTab;

        function getMessages() {
            function success(response) {
                vm.messages = response.data;
            }

            function failed(response) {
                Notification.error(response.data.message);
            }

            MailboxService
                .getMessages($rootScope.globals.currentUser.driver.email)
                .then(success, failed);
        }

        function openMessage($index) {
            vm.message = vm.messages[vm.tab][$index];
        }
        
        function setTab(tab) {
            vm.tab = tab;
            vm.message = null;
        }

    }

})();
 
(function () {
    'use strict';

    angular
        .module('main.mailbox', [])
        .config(config);

    config.$inject = ['$stateProvider'];
    function config($stateProvider) {
        
        $stateProvider
            .state('main.mailbox', {
                url: 'emails',
                templateUrl: '../views/mailbox/mailbox.html',
                controller: 'MailboxCtrl as vm',
                data: {
                    is_granted: ['ROLE_DRIVER']
                }
            });
        
    }
 
})();
 
(function () {
    'use strict';

    angular
        .module('main')
        .service('MailboxService', function ($http, MESSAGE_URL) {

            this.getAllMessages = function () {
                return $http.get(MESSAGE_URL);
            };
            
            this.getMessages = function (userId) {
                return $http.get(MESSAGE_URL + '/' + userId);
            };

            this.sendMessagesToUser = function (message) {
                return $http.post(MESSAGE_URL, message);
            };

        });  
})();  
(function () {
    'use strict';

    angular
        .module('main')
        .controller('MemberShipCtrl', MemberShipCtrl);

    function MemberShipCtrl(MemberShipService, $rootScope, $state) {
        var vm = this;

        init(); 

        vm.plans = [];
        vm.plan = {};
        
        vm.subscribePlan = subscribe;
        vm.getPlanList = getPlanList;

        var handler = StripeCheckout.configure({
            key: 'pk_live_2DfJupVZQEbB7MJ8P9qKrYPJ',
            image: 'dist/images/logo.png',
            locale: 'auto',
            token: function(token) {
                vm.plan.stripeToken = token.id;
                vm.plan.email = token.email;
                
                MemberShipService
                    .subscribePlan(vm.plan)
                    .then(function(data) {
                        $state.go('main.welcome');
                });
            }
        });

        function init() {
            getPlanList();
        }
        
        function getPlanList() {
            function success(response) {
                vm.plans = response.data.data;
            } 

            function error(response) {
                vm.plans = response.data.data;
            }

            MemberShipService
                .getPlanList()
                .then(success, error);
        }

        function subscribe(amount, name, planId) {
            vm.plan.planId = planId;

            handler.open({
                name: name ? name + ' Membership' : 'Membership',
                description: '',
                amount: amount
            });
        }
    }

})();
 
(function () {
    'use strict';

    angular
        .module('main.membership', [])
        .config(config);

    config.$inject = ['$stateProvider', '$urlRouterProvider', '$httpProvider'];
    function config($stateProvider, $urlRouterProvider, $httpProvider) {
        
        $stateProvider
            .state('main.membership', {
                url: 'membership',
                templateUrl: '../views/membership/membership.html',
                controller: 'MemberShipCtrl as vm'
            });
        
    }

})();
 
(function () {
    'use strict';

    angular
        .module('main')
        .service('MemberShipService', function ($http, PLAN_SUBSCRIBE_URL, PLAN_LIST_URL, $q, crAcl) {

            this.getPlanList = function () {
                return $http.get(PLAN_LIST_URL);
            };

            this.subscribePlan = function (planObject) {
                return $http.post(PLAN_SUBSCRIBE_URL, planObject);
            };

            this.getSubscriptionByDriverId = function (driverId) {
                return $http.get(PLAN_SUBSCRIBE_URL, {
                    params: {
                        driverId: driverId
                    }
                });
            };
        });  
})();   
(function () {
    'use strict';

    angular
        .module('main')
        .controller('PartnersCtrl', PartnersCtrl);

    function PartnersCtrl($log, CompaniesService, ngDialog, Notification) {
        var vm = this;


        vm.getPartnerCompanies = getPartnerCompanies;
        vm.companyDetailDialog = companyDetailDialog;
        
        vm.pcompanies = [];
        
        vm.params = {
            page: 1,
            limit: 9,
            city: null,
            zipcode: null,
            metroArea: null,
            empSizeRange: null,
            salseVolumeRange: null,
            isPartner :1 // getting only is partnered companies...
        };

        init();
        

        function init() {
            getPartnerCompanies();            
        }

        function getPartnerCompanies() {
            function success(response) {
                vm.pcompanies = response.data.data.companyData;
                vm.totalRecords = response.data.data.totalRecords;
                console.log('VM PARAMS',vm.pcompanies);
            }
 
            function failed(response) {
                $log.error(response.data.message);
            }
            CompaniesService
                .companyFilter(vm.params)
                .then(success, failed);
        }

        function companyDetailDialog(company) {

            var options = {
                templateUrl: '../views/partners/partners.detail.html',
                showClose: false,
                appendClassName: 'ngdialog-default',
                controller: 'PartnersCtrl as vm',
                data: {
                    company: company
                }
            };

            ngDialog.open(options);
        }

        

    }

})();
 
(function () {
    'use strict';

    angular
        .module('main.partners', [])
        .config(config);

    config.$inject = ['$stateProvider', '$urlRouterProvider', '$httpProvider'];
    function config($stateProvider, $urlRouterProvider, $httpProvider) {
        
        $stateProvider
            .state('main.partners', {
                url: 'partners',
                templateUrl: '../views/partners/partners.html',
                controller: 'PartnersCtrl as vm',
                data: {
                    is_granted: ['ROLE_DRIVER']
                }
            });
        
    }

})();
 
(function () {
    'use strict';

    angular
        .module('main')
        .controller('SearchCtrl', SearchCtrl);

    function SearchCtrl($log, $state, CompaniesService) {
        var vm = this;

        vm.params = {
            city: null,
            zipcode: null,
            metroArea: null,
            empSizeRange: null,
            salseVolumeRange: null
        };

        vm.init = function () {
            getStateList();
            getMetroAreaList();
            getEmployeeSizeList();
            getSalesVolumeList();
        };

        vm.getCompaniesByParams = getCompaniesByParams;

        function getCompaniesByParams() {
            $state.go('main.companies', vm.params);
        }

        function getSalesVolumeList() {
            function success(response) {
                vm.salesVolumeList = response.data.data;
            }

            function failed(response) {
                $log.error(response.data.message);
            }

            CompaniesService
                .getSalesVolumeList()
                .then(success, failed);
        }

        function getStateList() {
            function success(response) {
                vm.stateList = response.data.data;
            }

            function failed(response) {
                $log.error(response.data.message);
            }

            CompaniesService
                .getStateList()
                .then(success, failed);
        }

        function getMetroAreaList() {
            function success(response) {
                vm.metroAreaList = response.data.data;
            }

            function failed(response) {
                $log.error(response.data.message);
            }

            CompaniesService
                .getMetroAreaList()
                .then(success, failed);
        }

        function getEmployeeSizeList() {
            function success(response) {
                vm.employeeSizeList = response.data.data;
            }

            function failed(response) {
                $log.error(response.data.message);
            }

            CompaniesService
                .getEmployeeSizeList()
                .then(success, failed);
        }

    }

})();
 
(function () {
    'use strict';

    angular
        .module('main.search', [])
        .config(config);

    config.$inject = ['$stateProvider', '$urlRouterProvider', '$httpProvider'];
    function config($stateProvider, $urlRouterProvider, $httpProvider) {
        
        $stateProvider
            .state('main.search', {
                url: 'search',
                templateUrl: '../views/search/search.html',
                controller: 'SearchCtrl as vm'
            });
        
    }
 
})();
 
(function () {
    'use strict';

    angular
        .module('main.settings', [])
        .config(config);

    config.$inject = ['$stateProvider', '$urlRouterProvider', '$httpProvider'];
    function config($stateProvider, $urlRouterProvider, $httpProvider) {
        
        $stateProvider
            .state('main.settings', {
                url: 'settings',
                views: {
                    '@main': {
                        templateUrl: '../views/settings/settings.html',
                        controller: 'DriverAccountCtrl as vm'
                    }
                }
            });
          }
})(); 
 
(function () {
    'use strict';

    angular
        .module('main')
        .controller('SponsorshipsCtrl', SponsorshipsCtrl);

    function SponsorshipsCtrl($log, SponsorsService, Notification) {
        var vm = this;
        vm.sponsors = undefined;
        vm.params = {
            page: 1,
            limit: 10
        };
        vm.getSponsors = getSponsors;
        vm.getSponsors();
        function getSponsors() {

            function success(response) {
                vm.sponsors = response.data.data.sponsorData;
                vm.totalRecords = response.data.data.totalRecords;
            }
 
            function failed(response) {
                $log.error(response.data.message);
            }

            SponsorsService
                .getSponsors(vm.params)
                .then(success, failed);
        }

    }

})();
 
(function () {
    'use strict';

    angular
        .module('main.sponsorships', [])
        .config(config);

    config.$inject = ['$stateProvider', '$urlRouterProvider', '$httpProvider'];
    function config($stateProvider, $urlRouterProvider, $httpProvider) {
        
        $stateProvider
            .state('main.sponsorships', {
                url: 'sponsorships',
                templateUrl: '../views/sponsorships/sponsorships.html',
                controller: 'SponsorshipsCtrl as vm',
                data: {
                    is_granted: ['ROLE_DRIVER']
                }
            });
        
    }

})();
 
(function () {
    'use strict';

    angular
        .module('main.welcome', [])
        .config(config); 

    config.$inject = ['$stateProvider', '$urlRouterProvider', '$httpProvider'];
    function config($stateProvider, $urlRouterProvider, $httpProvider) {
        
        $stateProvider
            .state('main.welcome', {
                url: 'welcome',
                templateUrl: '../views/welcome/welcome.html'
            });
        
    }

})();
 
(function () {
    'use strict';

    angular
        .module('main')
        .controller('AdminDriversCtrl', AdminDriversCtrl);

    function AdminDriversCtrl(DriverService, Notification) {
        var vm = this;

        vm.drivers = [];

        vm.getDrivers = getDrivers;
        vm.removeDriver = removeDriver;

        function getDrivers() {
            function success(response) {
                vm.drivers = response.data.data;

            }

            function failed(response) {
                Notification.error(response.data.message);
            }

            DriverService
                .getAll()
                .then(success, failed);
        }

        function removeDriver(id) {
            function success(response) {
                getDrivers();
                Notification.success(response.data.message);

            }

            function failed(response) {
                Notification.error(response.data.message);
            }

            DriverService
                .delete(id)
                .then(success, failed);
        }

    }

})();
 
(function () {
    'use strict';

    angular
        .module('admin.drivers', [
            'admin.drivers.add',
            'admin.drivers.profile'
        ]) 
        .config(config);

    config.$inject = ['$stateProvider'];
    function config($stateProvider) {
        
        $stateProvider
            .state('admin.drivers', {
                url: '/drivers',
                templateUrl: '../views/admin/admin.drivers.html',
                controller: 'AdminDriversCtrl as vm',
                data: {
                    is_granted: ['ROLE_ADMIN']
                }
            });
        
    }
 
})();
 
(function () {
    'use strict';

    angular
        .module('main')
        .controller('AdminMailboxCtrl', AdminMailboxCtrl);
 
    function AdminMailboxCtrl(MailboxService, $scope, $state, $rootScope, $log) {
        var vm = this;

        init();

        vm.getAllMessages = getAllMessages;
        vm.openMessage = openMessage;
        vm.getMessages = getMessages;

        vm.messages = [];
        vm.tab = 'all';
        vm.counts = {}; 
        
        $scope.$state = $state;
        
        function init() {
            getAllMessages();
            // getMessages('sentbox');
            // getMessages('inbox');
        }

        function getAllMessages() {
            function success(response) {
                vm.messages = response.data.messages;
                vm.tab = 'all';
                vm.counts.all = response.data.messages.length;
                vm.message = null;
                $state.go('admin.mailbox.inbox');
            }

            function failed(response) {
                $log.error(response.data.message);
            }

            MailboxService
                .getAllMessages()
                .then(success, failed);
        }

        function getMessages(tab) {
            function success(response) {
                vm.messages = response.data[tab];
                vm.tab = tab;
                vm.counts[tab] = response.data[tab].length;
                vm.message = null;
                $state.go('admin.mailbox.inbox');
            }

            function failed(response) {
                $log.error(response.data.message);
            }

            MailboxService
                .getMessages($rootScope.globals.currentUser.email)
                .then(success, failed);
        }

        function openMessage($index) {
            vm.message = vm.messages[$index];
        }

    }

})();
 
(function () {
    'use strict';

    angular
        .module('admin.mailbox', [
            'admin.mailbox.compose'
        ])
        .config(config);
 
    config.$inject = ['$stateProvider'];
    function config($stateProvider) {
        
        $stateProvider
            .state('admin.mailbox', {
                url: '/mailbox',
                templateUrl: '../views/admin/mailbox/admin.mailbox.html',
                controller: 'AdminMailboxCtrl as vm',
                data: {
                    is_granted: ['ROLE_ADMIN']
                }
            });
        
    }
 
})();
 
(function () {
    'use strict';

    angular
        .module('main')
        .controller('AdminPlansCtrl', AdminPlansCtrl);

    function AdminPlansCtrl(AdminService, Notification, $log) {
        var vm = this;

        getPlans();
        
        function getPlans() {
            function success(response) {
                $log.info(response.data);
            }

            function failed(response) {
                $log.error(response.data.message);
            }

            AdminService
                .getPlanList()
                .then(success, failed);
        }
        
    }

})();
 
(function () {
    'use strict';

    angular
        .module('admin.plans', []) 
        .config(config);

    config.$inject = ['$stateProvider'];
    function config($stateProvider) {
        
        $stateProvider
            .state('admin.plans', {
                url: '/plans',
                templateUrl: '../views/admin/admin.plans.html',
                controller: 'AdminPlansCtrl as vm',
                data: {
                    is_granted: ['ROLE_ADMIN']
                }
            });
        
    }
 
})();
 
(function () {
    'use strict';

    angular
        .module('main')
        .controller('AdminCompaniesCtrl', AdminCompaniesCtrl);
 
    function AdminCompaniesCtrl(UPLOAD_CSV_URL, DriverService, CompaniesService, Notification, $rootScope, $log) {
        var vm = this;

        init();

        vm.uploadCompleted = uploadCompleted;
        vm.uploadError = uploadError;
        vm.getCompanies = getCompanies;
        vm.uploadCSV = uploadCSV;
        vm.isPartner = isPartner;
        
        vm.flow = {};
        vm.flowConfig = {
            target: UPLOAD_CSV_URL,
            singleFile: true,
            testChunks: false,
            headers: {
                'Authorization' : $rootScope.globals.currentUser.token
            }
        };

        vm.params = {
            page: 1,
            limit: 10,
            city: null,
            zipcode: null,
            metroArea: null,
            empSizeRange: null,
            salseVolumeRange: null
        };

        vm.paramsDriversFavoriteCompany = {
            page: 1,
            limit: 10,
            search: '596e6112523b70105ba9e8cd'
        };

        vm.companies = [];
        vm.columns = [];

        function init() {
            getStateList();
            getMetroAreaList();
            getEmployeeSizeList();
            getSalesVolumeList();
        }

        function getCompanies() {
            function success(response) {
                vm.companies = response.data.data.companyData;
                vm.totalRecords = response.data.data.totalRecords;
                vm.columns = []; //fixed columns repeat issue  
                if (vm.companies.length) 
                    for (var key in vm.companies[0]) 
                        if (key !== '_id')
                            vm.columns.push(key);
            }
 
            function failed(response) {
                $log.error(response.data.message);
            }

            CompaniesService
                .companyFilter(vm.params)
                .then(success, failed);
        }

        function uploadCSV() {
            CompaniesService
                .uploadCSV(vm.flow.files[0].file)
                .then(function(response){

                    uploadCompleted(response.message);

                }, function(response){
                    uploadError(response.message);
                }, function(progress){
                    vm.uploadProgress = progress;
                });
        }
        
        function uploadCompleted(message) {
            Notification.success(message);
            vm.flow.cancel();
            getCompanies();
            $log.info(message);
            vm.uploadProgress = 0;
        }

        function uploadError(message) {
            Notification.error(message);
            $log.error(message);
        }

        function getSalesVolumeList() {
            function success(response) {
                vm.salesVolumeList = response.data.data;
            }

            function failed(response) {
                $log.error(response.data.message);
            }

            CompaniesService
                .getSalesVolumeList()
                .then(success, failed);
        }

        function getStateList() {
            function success(response) {
                vm.stateList = response.data.data;
            }

            function failed(response) {
                $log.error(response.data.message);
            }

            CompaniesService
                .getStateList()
                .then(success, failed);
        }

        function getMetroAreaList() {
            function success(response) {
                vm.metroAreaList = response.data.data;
            }

            function failed(response) {
                $log.error(response.data.message);
            }

            CompaniesService
                .getMetroAreaList()
                .then(success, failed);
        }

        function getEmployeeSizeList() {
            function success(response) {
                vm.employeeSizeList = response.data.data;
            }

            function failed(response) {
                $log.error(response.data.message);
            }

            CompaniesService
                .getEmployeeSizeList()
                .then(success, failed);
        }

        function isPartner(rowIndex,colIndex) {
            
            function success(response) {
                getCompanies();
                Notification.success(response.data.message);
            }

            function failed(response) {
                $log.error(response.data.message);
            }

            CompaniesService.
                toggleIsPartner(vm.companies[rowIndex]._id)
                .then(success,failed);
            console.log('isPartner is clicked.',vm.companies[rowIndex]._id);
        }

    }

})();
 
(function () {
    'use strict';

    angular
        .module('admin.companies', []) 
        .config(config);

    config.$inject = ['$stateProvider'];
    function config($stateProvider) {
        
        $stateProvider
            .state('admin.companies', {
                url: '/companies',
                templateUrl: '../views/admin/admin.companies.html',
                controller: 'AdminCompaniesCtrl as vm',
                data: {
                    is_granted: ['ROLE_ADMIN']
                }
            });
        
    }
 
})();
 
(function () {
    'use strict';

    angular
        .module('main')
        .controller('AdminLoginCtrl', AdminLoginCtrl);

    function AdminLoginCtrl(AuthService, Notification, crAcl, $state) {
        var vm = this;

        vm.credentials = {};
        vm.loginForm = null;
        
        vm.login = login;

        function login() {
            function success(response) {
                response.data.data.role = 'ROLE_ADMIN';

                AuthService
                    .setCredentials(response.data.data);

                crAcl
                    .setRole('ROLE_ADMIN');

                $state.go('admin.drivers');
            }

            function failed(response) {
                Notification.error(response.data.message);
            }

            if (vm.loginForm.$valid)
                AuthService
                    .login(vm.credentials)
                    .then(success, failed);
        }

    }

})();
 
(function () {
    'use strict';

    angular
        .module('admin.login', []) 
        .config(config);

    config.$inject = ['$stateProvider', '$urlRouterProvider', '$httpProvider'];
    function config($stateProvider, $urlRouterProvider, $httpProvider) {
        
        $stateProvider
            .state('admin.login', {
                url: '/login',
                views: {
                    '@': {
                        templateUrl: '../views/admin/admin.login.html',
                        controller: 'AdminLoginCtrl as vm'
                    }
                },
                onEnter: function (AuthService) {
                    AuthService.clearCredentials();
                }
            }); 
        
    }

})();
 
(function () {
    'use strict';

    angular
        .module('main')
        .controller('AdminSponsorCtrl', AdminSponsorCtrl);
 
    function AdminSponsorCtrl(SponsorsService, Notification, $rootScope, $log) {
        var vm = this;
        vm.sponsors = undefined;
        vm.params = {
            page: 1,
            limit: 10
        };
        vm.getSponsors = getSponsors;
        vm.getSponsors();
        function getSponsors() {
            function success(response) {
                vm.sponsors = response.data.data.sponsorData;
                vm.totalRecords = response.data.data.totalRecords;
            }
 
            function failed(response) {
                $log.error(response.data.message);
            }

            SponsorsService
                .getSponsors(vm.params)
                .then(success, failed);
        }

    }   

})();
 
(function () {
    'use strict';

    angular
        .module('admin.sponsors', [
            'admin.sponsors.add'
        ]) 
        .config(config);

    config.$inject = ['$stateProvider'];
    function config($stateProvider) {
        
        $stateProvider
            .state('admin.sponsors', {
                url: '/sponsors',
                templateUrl: '../views/admin/admin.sponsors.html',
                controller: 'AdminSponsorCtrl as vm',
                data: {
                    is_granted: ['ROLE_ADMIN']
                }
            });
        
    }
 
})();
 
(function () {
    'use strict';

    angular
        .module('main')
        .service('SponsorsService', function ($http,
                                                    $q,
                                                    $rootScope,
                                                    SPONSOR_URL
                                                    ) {

            this.createSponsorship = function (sponsor) {
                return $http.post(SPONSOR_URL, sponsor);
            };

            this.getSponsors = function (params) {
                return $http.get(SPONSOR_URL , {params: params});
            };

        });  
})();  
(function () {
    'use strict';

    angular
        .module('main')
        .controller('ResetPasswordPasswordCtrl', ResetPasswordPasswordCtrl);

    function ResetPasswordPasswordCtrl(DriverService, Notification, $stateParams) {
        var vm = this;

        vm.data = {
            token: $stateParams.token,
            password: null
        };
        
        vm.reset = reset;

        function reset() {
            function success(response) {
                Notification.success(response.data.message);
            }

            function failed(response) {
                Notification.error(response.data.message);
            }

            DriverService
                .resetPassword(vm.data)
                .then(success, failed);
        }
        
    }

})();
 
(function () {
    'use strict';

    angular
        .module('main.auth.resetPassword', [])
        .config(config);

    config.$inject = ['$stateProvider', '$urlRouterProvider', '$httpProvider'];
    function config($stateProvider, $urlRouterProvider, $httpProvider) {
        
        $stateProvider
            .state('main.auth.resetPassword', {
                url: 'resetPassword?token',
                views: {
                    '@main': {
                        templateUrl: '../views/auth/auth.resetPassword.html',
                        controller: 'ResetPasswordPasswordCtrl as vm'
                    }
                }
            });
        
    }

})();
 
(function () {
    'use strict';

    angular
        .module('main')
        .controller('AuthForgotPasswordCtrl', AuthForgotPasswordCtrl);

    function AuthForgotPasswordCtrl(DriverService, Notification) {
        var vm = this;

        vm.send = send;

        function send() {
            function success(response) {
                Notification.success(response.data.message);
            }

            function failed(response) {
                Notification.error(response.data.message);
            }

            DriverService
                .forgotPassword(vm.email)
                .then(success, failed);
        }
        
    }

})();
 
(function () {
    'use strict';

    angular
        .module('main.auth.forgotPassword', [])
        .config(config);

    config.$inject = ['$stateProvider', '$urlRouterProvider', '$httpProvider'];
    function config($stateProvider, $urlRouterProvider, $httpProvider) {
        
        $stateProvider
            .state('main.auth.forgotPassword', {
                url: 'forgotPassword',
                views: {
                    '@main': {
                        templateUrl: '../views/auth/auth.forgotPassword.html',
                        controller: 'AuthForgotPasswordCtrl as vm'
                    }
                }
            });
        
    }

})();
 
(function () {
    'use strict';

    angular
        .module('main')
        .controller('AuthSignInCtrl', AuthSignInCtrl);

    function AuthSignInCtrl(DriverService, AuthService, MemberShipService, Notification, crAcl, $state, $stateParams) {
        var vm = this;

        vm.loginForm = null;

        vm.credentials = {}; 
        
        vm.alert = null;

        vm.login = login;
        
        init();
        
        function init() {
            if ($stateParams.verifyUserAccount)
                verifyUserAccount();
        }

        function login() {
            function success(response) {
                response.data.data.role = 'ROLE_DRIVER';

                AuthService
                    .setCredentials(response.data.data);
                
                crAcl
                    .setRole('ROLE_DRIVER');

                getSubscriptionByDriverId(response.data.data.driver._id)
            }

            function failed(response) {
                Notification.error(response.data.message);
            }

            if (vm.loginForm.$valid)
                DriverService
                    .login(vm.credentials)
                    .then(success, failed);
        }

        function getSubscriptionByDriverId(id) {
            function success(response) {
                if (response.data.data.length)
                    $state.go('main.search');
                else
                    $state.go('main.membership');
            }

            MemberShipService
                .getSubscriptionByDriverId(id)
                .then(success);
        }

        function verifyUserAccount() {
            function success(response) {
                vm.alert = {
                    type: 'success',
                    text: response.data.message
                }
            }
            
            function error(response) {
                vm.alert = {
                    type: 'error',
                    text: response.data.message
                }
            }

            AuthService
                .verifyUserAccount($stateParams.verifyUserAccount)
                .then(success, error);
        }
        
        

    }

})();
 
(function () {
    'use strict';

    angular
        .module('main.auth.signin', [])
        .config(config);

    config.$inject = ['$stateProvider', '$urlRouterProvider', '$httpProvider'];
    function config($stateProvider, $urlRouterProvider, $httpProvider) {
        
        $stateProvider
            .state('main.auth.signin', {
                url: 'signin?verifyUserAccount',
                views: {
                    '@main': { 
                        templateUrl: '../views/auth/auth.signin.html',
                        controller: 'AuthSignInCtrl as vm'
                    }
                }
            });
        
    }

})();
 
(function () {
    'use strict';

    angular
        .module('main')
        .controller('AuthSignUpCtrl', AuthSignUpCtrl);

    function AuthSignUpCtrl(DriverService, Notification, AuthService, crAcl, $state) {
        var vm = this;

        vm.registerForm = null;
        vm.usernameIsExist = false;
        vm.checkUsernameShow = false;

        vm.driver = {};

        vm.register = register;
        vm.checkUsername = checkUsername;

        function register() {
            function success() {
                login();
            } 

            function failed(response) {
                if (response.status === 400)
                    switch (response.data.code) {
                        case 11000 : Notification.error('This email is already exist'); break;
                    }
                else
                    Notification.error(response.data.message);
            }

            vm.driver.birthdate = new Date();

            if (vm.registerForm.$valid)
                if (vm.driver.password === vm.driver.verifyPassword
                    && !vm.usernameIsExist)
                    DriverService
                        .register(vm.driver)
                        .then(success, failed);

                else if (vm.driver.password !== vm.driver.verifyPassword)
                    Notification.error('Verify Password isn\'t correct');

                else if (vm.usernameIsExist)
                    Notification.error('Username already used');
        }

        function login() {
            function success(response) {
                response.data.data.role = 'ROLE_DRIVER';

                AuthService
                    .setCredentials(response.data.data);

                crAcl
                    .setRole('ROLE_DRIVER');

                $state.go('main.search');
            }

            function failed(response) {
                Notification.error(response.data.message);
            }

            DriverService
                .login(vm.driver)
                .then(success, failed);
        }
        
        function checkUsername() {
            function success(response) {
                vm.usernameIsExist = !!response.data.data;
                vm.checkUsernameShow = true;

                console.log(vm.usernameIsExist);
            }

            function failed(response) {
            }

            if (vm.driver.userName)
                DriverService
                    .getProfileByUsername(vm.driver.userName)
                    .then(success, failed);
            else
                vm.checkUsernameShow = false;
        }

    }

})();
 
(function () {
    'use strict';

    angular
        .module('main.auth.signup', [])
        .config(config);

    config.$inject = ['$stateProvider', '$urlRouterProvider', '$httpProvider'];
    function config($stateProvider, $urlRouterProvider, $httpProvider) {
        
        $stateProvider
            .state('main.auth.signup', {
                url: 'signup',
                views: {
                    '@main': {
                        templateUrl: '../views/auth/auth.signup.html',
                        controller: 'AuthSignUpCtrl as vm'
                    }
                },
                onEnter: function (AuthService) {
                    AuthService.clearCredentials();
                }
            });
        
    }

})();
 
(function () {
  'use strict';

  angular
    .module('main')
    .controller('verifyAccountCtrl', verifyAccountCtrl);

  function verifyAccountCtrl(DriverService, Notification, $stateParams, $state) {
    var vm = this;

    vm.data = {
      token: $stateParams.token
    };

    vm.redirectToLogin = redirectToLogin;
    vm.verifyUserAccount = verifyUserAccount;
    vm.message = "Verification is in progress";
    function redirectToLogin() {
      $state.go('main.auth.signin');
    };

    function verifyUserAccount() {
      function success(response) {
        vm.message = "Your account has been successfully verified";
        Notification.success(response.data.message);
      }

      function failed(response) {
        vm.message = "Error while verifying your account";
        Notification.error(response.data.message);
      }

      DriverService
        .verifyDriverAccount(vm.data)
        .then(success, failed);
    }

  }

})();

(function () {
  'use strict';

  angular
      .module('main.auth.verifyAccount', [])
      .config(config);

  config.$inject = ['$stateProvider', '$urlRouterProvider', '$httpProvider'];
  function config($stateProvider, $urlRouterProvider, $httpProvider) {

      $stateProvider
          .state('main.auth.verifyAccount', {
              url: 'verifyAccount/:token',
              views: {
                  '@main': {
                      templateUrl: '../views/auth/auth.verifyAccount.html',
                      controller: 'verifyAccountCtrl as vm'
                  }
              }
          });

  }

})();

(function () {
    'use strict';

    angular
        .module('main')
        .controller('CompaniesContactCtrl', CompaniesContactCtrl);

    function CompaniesContactCtrl($log, $rootScope, $scope, ngDialog, MailboxService, CompaniesService, DriverService, Notification) {
        var vm = this;

        vm.message = {
            senderMailId: $rootScope.globals.currentUser.driver.email,
            receiverMailId: $scope.ngDialogData.company['EXECUTIVE EMAIL'],
            subject: $scope.ngDialogData.company['COMPANY NAME'] + ' - ' + $rootScope.globals.currentUser.driver.firstName + ' ' + $rootScope.globals.currentUser.driver.lastName,
            messageContents: null,
            profileAttached: false
        };

        vm.send = sendMessage;

        function sendMessage() {
            function success(response) {
                Notification.success(response.data.message);
                ngDialog.close();
            }

            function failed(response) {
                Notification.error(response.data.message);
            }

            MailboxService
                .sendMessagesToUser(vm.message)
                .then(success, failed);
        }

    }

})();

(function () {
    'use strict';

    angular
        .module('main')
        .controller('DriverAccountCtrl', DriverAccountCtrl);

    function DriverAccountCtrl(DriverService,
                               AuthService,
                               Notification,
                               MemberShipService,
                               ngDialog,
                               $scope,
                               $rootScope,
                               $state,
                               DRIVER_MEDIA_URL,
                               UPLOAD_PDF_URL) {
        var vm = this;

        vm.formDriver = null;
        vm.subscriptionDate = null;

        vm.driver = {};
        vm.name = 'name';
        vm.flow = {};
        vm.flowConfig = {
            target: DRIVER_MEDIA_URL,
            singleFile: true,
            testChunks: false
        };

        vm.flowPdf = {};
        vm.flowPdfConfig = {
            target: UPLOAD_PDF_URL,
            singleFile: true,
            testChunks: false
        };

        vm.seriesTypes = [
            {
                name: 'test1',
                value: 1
            },
            {
                name: 'test2',
                value: 2
            },
            {
                name: 'test3',
                value: 3
            },
            {
                name: 'test4',
                value: 4
            },
            {
                name: 'test5',
                value: 5
            }
        ];
        vm.uploadProgress = 0;

        vm.rows = [
            {
                seriesType: 1,
                carNumber: 554654
            }
        ];

        vm.originImage = '';
        vm.profilePic = '';

        vm.getDriver = getDriver;
        vm.update = update;
        vm.addRow = addRow;
        vm.fileAdded = fileAdded;
        vm.upload = upload;
        vm.rotate = rotate;
        vm.getSubscription = getSubscription;
        vm.updatePaymentDetails = updatePaymentDetails;

        function fileAdded($flow, $file) {
            var reader = new FileReader();

            reader.onload = function (evt) {
                $scope.$apply(function($scope){
                    $scope.originImage = evt.target.result;
                });
            };
            reader.readAsDataURL($file.file);
        }

        function addRow() {
            vm.rows.push({
                seriesType: null,
                carNumber: null
            });
        }

        //alert('here i call driver');

        function getDriver() {

            function success(response) {
                vm.driver = response.data.data;

                vm.driver.birthdate = new Date(vm.driver.birthdate);
            }

            function failed(response) {
                Notification.error(response.data.message);
            }

            DriverService
                .getProfile(AuthService.getCurrentUser().driver._id)
                .then(success, failed);

             DriverService
                .getCard().then(function(response){
                    var m = 1;
                    var data = response.data[0];
                    var cDetails = data.cardId[0];
                    var m = cDetails.expiringMonth;
                    var y = cDetails.expiringYear;
                    cDetails.expireData = m +'/'+ getLastMonthDay(m,y)+'/'+ y;
                    
                    var pDetails = {
                                     'isPaymentApplied' : data.isPaymentApplied,
                                     'nextBillDate' : data.nextBillDate,
                                     'createdOn' : data.createdOn
                                    };
                    

                    vm.driver.card = cDetails;
                    vm.driver.payament = pDetails;

                    vm.formPayment = cDetails;
                });

            DriverService.getSubscribedPlan()
                .then(function(response){
                    
                    vm.subPlan = response.data.data.planId;
                    //console.log('->',response.message);
                });

            DriverService.getDriverPlanList()
                .then(function(response){
                    
                    vm.plansList = response.data.data;
                    //console.log('->',response.message);
                });
        }


         function updatePaymentDetails(){
            DriverService.updatePaymentDetails(vm.formPayment._id, vm.formPayment)
            .then(function(response){
                Notification.success('Updated');
                // console.log('->',response.message);
            });
            var newPlan = {planId : vm.subPlan._id};
            console.log('newPlan',newPlan);
            DriverService.changePlan(newPlan)
            .then(function(response){
                //Notification.success('Updated plan');
                // console.log('->',response.message);
            });

            
            console.log('hello worl',vm.formPayment,vm.subPlan._id);    
        } 


        function getLastMonthDay(m,y){
            var thirtyOneDays = [1,3,5,7,8,10,12];
            var thirtyDays = [4,6,9,11];
            
                    
            if(thirtyOneDays.indexOf(m) >= 0)
            {
                return 31;            
            }
            else
            {
                if(thirtyDays.indexOf(m) >= 0){
                    return 30
                }else
                {

                   if(m == 2){
                        if(leapYear(y)){

                            return 29;

                        }else{
                            return 28;    
                        }
                   } 
                }
            }
            return 0;
        }

        function leapYear(year)
        {
          return ((year % 4 == 0) && (year % 100 != 0)) || (year % 400 == 0);
        }

        function update(image, pdf) {
            function success() {
                Notification.success('Updated');
                vm.flow.cancel();
                ngDialog.close();
                $state.go('main.driver.account', {}, {reload: $state.is('main.driver.account.uploadAvatar')})
            }

            function failed(response) {
                Notification.error(response.data.message);
            }
            
            if (image)
                vm.driver.profilePic = image.fileName;

            if (pdf)
                vm.driver.sponsorshipPitchAttachment = JSON.parse(pdf).fileName;

            console.log($state.is('main.driver.account') &&
                vm.formDriver.$valid);

            if ($state.is('main.driver.account') &&
                vm.formDriver.$valid)
                DriverService
                    .update(vm.driver)
                    .then(success, failed);
            else if ($state.is('main.driver.account.uploadAvatar'))
                DriverService
                    .update(vm.driver)
                    .then(success, failed);
        }

        function getSubscription() {
            function success(response) {
                vm.subscriptionDate = new Date(response.data.data[0].createdOn).getDate();
            }

            MemberShipService
                .getSubscriptionByDriverId($rootScope.globals.currentUser.driver._id)
                .then(success);
        }

        function update(image, pdf) {
            function success() {
                Notification.success('Updated');
                vm.flow.cancel();
                ngDialog.close();
                $state.go('main.driver.account', {}, { reload: true });
            }

            function failed(response) {
                Notification.error(response.data.message);
            }
            
            if (image)
                vm.driver.profilePic = image.fileName;

            if (pdf)
                vm.driver.sponsorshipPitchAttachment = JSON.parse(pdf).fileName;

            console.log($state.is('main.driver.account') &&
                vm.formDriver.$valid);

            if ($state.is('main.driver.account') &&
                vm.formDriver.$valid || pdf)
                DriverService
                    .update(vm.driver)
                    .then(success, failed);
            else if ($state.is('main.driver.account.uploadAvatar'))
                DriverService
                    .update(vm.driver)
                    .then(success, failed);
        }

        function upload() {
            function success(data) {
                update(data);
            }

            function failed() {

            }
            
            function progress(progress) {
                vm.uploadProgress = progress;
            }
            
            DriverService
                .uploadAvatar(vm.profilePic)
                .then(success, failed, progress);
        }

        function rotate() {
            var canvas = document.createElement("canvas");
            var img = new Image();
            var cw = img.width, ch = img.height, cx = 0, cy = 0, deg = 0;

            img.onload = function() {
                var context = canvas.getContext("2d");

                cw = img.height;
                ch = img.width;
                cx =- img.width;
                deg = 270;

                canvas.width = cw;
                canvas.height = ch;

                context.rotate(deg * Math.PI / 180);
                context.drawImage(this, cx, cy);

                $scope.$apply(function($scope){
                    $scope.originImage = canvas.toDataURL("image/png");
                });

            };

            img.src = $scope.originImage;

        }

    }

})();
 
(function () {
    'use strict';

    angular
        .module('main.driver.account', [
            'main.driver.account.uploadAvatar'
        ])
        .config(config);

    config.$inject = ['$stateProvider', '$urlRouterProvider', '$httpProvider'];
    function config($stateProvider, $urlRouterProvider, $httpProvider) {
        
        $stateProvider
            .state('main.driver.account', {
                url: 'account',
                views: {
                    '@main': {
                        templateUrl: '../views/driver/account/driver.account.html',
                        controller: 'DriverAccountCtrl as vm'
                    }
                },
                data: {
                    is_granted: ['ROLE_DRIVER']
                }
            });
        
    }

})();
 
(function () {
    'use strict';

    angular
        .module('main')
        .controller('DriverProfileCtrl', DriverProfileCtrl);

    function DriverProfileCtrl(DriverService,
                               $stateParams,
                               Notification,
                               ngDialog,
                               $scope,
                               $state,
                               DRIVER_MEDIA_URL,
                               UPLOAD_PDF_URL) {
        var vm = this;

        vm.formDriver = null;

        vm.driver = {};

        vm.flow = {};
        vm.flowConfig = {
            target: DRIVER_MEDIA_URL,
            singleFile: true,
            testChunks: false
        };

        vm.flowPdf = {};
        vm.flowPdfConfig = {
            target: UPLOAD_PDF_URL,
            singleFile: true,
            testChunks: false
        };

        vm.seriesTypes = [
            {
                name: 'test1',
                value: 1
            },
            {
                name: 'test2',
                value: 2
            },
            {
                name: 'test3',
                value: 3
            },
            {
                name: 'test4',
                value: 4
            },
            {
                name: 'test5',
                value: 5
            }
        ];

        vm.uploadProgress = 0;
        vm.uploadPdfProgress = 0;

        vm.rows = [
            {
                seriesType: 1,
                carNumber: 554654
            }
        ];

        vm.originImage = '';
        vm.profilePic = '';

        vm.getDriver = getDriver;
        vm.update = update;
        vm.addRow = addRow;
        vm.fileAdded = fileAdded;
        vm.upload = upload;
        vm.rotate = rotate;
        vm.uploadPdf = uploadPdf;

        function fileAdded($flow, $file) {
            var reader = new FileReader();

            reader.onload = function (evt) {
                $scope.$apply(function($scope){
                    $scope.originImage = evt.target.result;
                });
            };
            reader.readAsDataURL($file.file);
        }

        function addRow() {
            vm.rows.push({
                seriesType: null,
                carNumber: null
            });
        }

        function getDriver() {
            function success(response) {
                vm.driver = response.data.data;

                vm.driver.birthdate = new Date(vm.driver.birthdate);
            }

            function failed(response) {
                Notification.error(response.data.message);
            }

            DriverService
                .getProfileByUsername($stateParams.username)
                .then(success, failed);
        }

        function update(image) {
            function success() {
                Notification.success('Updated');
                vm.flow.cancel();
                ngDialog.close();
                $state.go('main.driver.profile', {}, {reload: $state.is('main.driver.profile.uploadAvatar')})
            }

            function failed(response) {
                Notification.error(response.data.message);
            }
            
            if (image)
                vm.driver.profilePic = image.fileName;

            if ($state.is('main.driver.profile') &&
                vm.formDriver.$valid)
                DriverService
                    .update(vm.driver)
                    .then(success, failed);
            else if ($state.is('main.driver.profile.uploadAvatar'))
                DriverService
                    .update(vm.driver)
                    .then(success, failed);
        }

        function upload() {
            function success(data) {
                update(data);
            }

            function failed() {

            }
            
            function progress(progress) {
                vm.uploadProgress = progress;
            }
            
            DriverService
                .uploadAvatar(vm.profilePic)
                .then(success, failed, progress);
        }

        function rotate() {
            var canvas = document.createElement("canvas");
            var img = new Image();
            var cw = img.width, ch = img.height, cx = 0, cy = 0, deg = 0;

            img.onload = function() {
                var context = canvas.getContext("2d");

                cw = img.height;
                ch = img.width;
                cx =- img.width;
                deg = 270;

                canvas.width = cw;
                canvas.height = ch;

                context.rotate(deg * Math.PI / 180);
                context.drawImage(this, cx, cy);

                $scope.$apply(function($scope){
                    $scope.originImage = canvas.toDataURL("image/png");
                });

            };

            img.src = $scope.originImage;

        }

        function uploadPdf($file) { 
            function success(data) {
                update(data);
            }

            function failed() {

            }

            function progress(progress) {
                vm.uploadPdfProgress = progress;
            }

            // DriverService
            //     .uploadPdf($file)
            //     .then(success, failed, progress);
            vm.flowPdf.upload();
        }

    }

})();
 
(function () {
    'use strict';

    angular
        .module('main.driver.profile', [])
        .config(config);

    config.$inject = ['$stateProvider', '$urlRouterProvider', '$httpProvider'];
    function config($stateProvider, $urlRouterProvider, $httpProvider) {
        
        $stateProvider
            .state('main.driver.profile', {
                url: ':username',
                views: {
                    '@main': {
                        templateUrl: '../views/driver/profile/driver.profile.html',
                        controller: 'DriverProfileCtrl as vm'
                    }
                }
            });
        
    }

})();
 
(function () {
    'use strict';

    angular
        .module('main')
        .controller('AdminDriversAddCtrl', AdminDriversAddCtrl);

    function AdminDriversAddCtrl(DriverService, Notification, $state) {
        var vm = this;

        vm.driver = {}; 
        vm.generalForm = null;
        
        vm.create = create;

        function create() {
            function success(response) {
                Notification.success(response.data.message);
                $state.go('admin.drivers');
            }

            function failed(response) {
                Notification.error(response.data.message);
            }

            if (vm.generalForm.$valid)
                DriverService
                    .register(vm.driver)
                    .then(success, failed);
        }

    }

})();
 
(function () {
    'use strict';

    angular
        .module('admin.drivers.add', []) 
        .config(config);

    config.$inject = ['$stateProvider'];
    function config($stateProvider) {
        
        $stateProvider
            .state('admin.drivers.add', {
                url: '/add',
                views: {
                    '@admin': {
                        templateUrl: '../views/admin/admin.drivers.add.html',
                        controller: 'AdminDriversAddCtrl as vm'
                    }
                },
                data: {
                    is_granted: ['ROLE_ADMIN']
                }
            });
        
    }

})();
 
(function () {
    'use strict';

    angular
        .module('main')
        .controller('AdminDriversProfileCtrl', AdminDriversProfileCtrl);

    function AdminDriversProfileCtrl(DriverService, Notification, $stateParams, DRIVER_MEDIA_URL) {
        var vm = this;

        vm.formDriver = null;

        vm.driver = {};

        vm.flow = {};
        vm.flowConfig = {
            target: DRIVER_MEDIA_URL,
            singleFile: true,
            testChunks: false
        };

        vm.getProfile = getProfile;
        vm.update = update;

        function getProfile() {
            function success(response) {
                vm.driver = response.data.data;

                vm.driver.birthdate = new Date(vm.driver.birthdate);
            }

            function failed(response) {
                Notification.error(response.data.message);
            }

            DriverService
                .getProfile($stateParams.id)
                .then(success, failed);
        }

        function update($message) {
            function success() {
                Notification.success('Updated');
                vm.flow.cancel();
            }

            function failed(response) {
                Notification.error(response.data.message);
                vm.flow.cancel();
            }

            if ($message)
                vm.driver.profilePic = JSON.parse($message).fileName;

            if (vm.formDriver.$valid)
                DriverService
                    .update(vm.driver)
                    .then(success, failed);
        }

    }

})();
 
(function () {
    'use strict';
 
    angular
        .module('admin.drivers.profile', [
            'admin.drivers.profile.tabs'
        ]) 
        .config(config);

    config.$inject = ['$stateProvider'];
    function config($stateProvider) {
        
        $stateProvider
            .state('admin.drivers.profile', {
                url: '/profile/:id',
                views: {
                    '@admin': {
                        templateUrl: '../views/admin/profile/admin.drivers.profile.html',
                        controller: 'AdminDriversProfileCtrl as vm'
                    }
                },
                abstract: true,
                data: {
                    is_granted: ['ROLE_ADMIN']
                }
            });
        
    }

})();
 
(function () {
    'use strict';

    angular
        .module('main')
        .controller('AdminMailboxComposeCtrl', AdminMailboxComposeCtrl);

    function AdminMailboxComposeCtrl(DriverService, MailboxService, Notification, $rootScope) {
        var vm = this;

        vm.drivers = [];
        vm.driversForSend = [];
        vm.message = {};

        vm.getDrivers = getDrivers;
        vm.send = send;
        function getDrivers() {
            function success(response) {
                vm.drivers = response.data.data;        
                $('.scrollbar-dynamic').scrollbar({
                    scrolly: false
                });
            }

            function failed(response) {
                Notification.error(response.data.message);
            }

            DriverService
                .getAll()
                .then(success, failed);
        }
        
        function send() {
            vm.driversForSend.forEach(sendMessage);
            console.log(vm.driversForSend);
        }
        
        function sendMessage(driver) {
            function success(response) {
                Notification.success(response.data.message);
            }

            function failed(response) {
                if (response.data.code)
                    Notification.error(response.data.errmsg);
                else
                    Notification.error(response.data.message);
            }

            vm.message.senderMailId = $rootScope.globals.currentUser.email;
            vm.message.receiverMailId = driver.email;

            MailboxService
                .sendMessagesToUser(vm.message)
                .then(success, failed);
        }

    }

})();
 
(function () {
    'use strict';

    angular
        .module('admin.mailbox.compose', [])
        .config(config);

    config.$inject = ['$stateProvider'];
    function config($stateProvider) {
        
        $stateProvider
            .state('admin.mailbox.compose', {
                url: '/compose',
                views: {
                    '@admin': {
                        templateUrl: '../views/admin/mailbox/admin.mailbox.compose.html',
                        controller: 'AdminMailboxComposeCtrl as vm'
                    }
                },
                data: {
                    is_granted: ['ROLE_ADMIN']
                }
            });
        
    }
 
})();
 
(function () {
    'use strict';

    angular
        .module('main')
        .controller('AdminSponsorsAddCtrl', AdminSponsorsAddCtrl);

    function AdminSponsorsAddCtrl(SponsorsService,CompaniesService ,Notification, $state) {
        var vm = this;

        vm.sponsor = {};
        vm.partnerCompanies = {};
        vm.creatSponsorship = creatSponsorship;
        
        getPartnerCompanies(); /*calling partner companies for dropdown list*/

        function creatSponsorship(){
            
            function success(response) {
                Notification.success(response.data.message);
                vm.sponsor = {};
            }

            function failed(response) {
                Notification.error(response.data.message);
            }

            SponsorsService
                .createSponsorship(vm.sponsor)
                .then(success, failed);
        }

        function getPartnerCompanies(){
            
            function success(response) {
                vm.partnerCompanies = response.data.data;
            }

            function failed(response) {
                // Notification.error(response.data.message);
            }

            CompaniesService
                .getPartnerCompanies()
                .then(success, failed);   
        }           

    }

})();
 
(function () {
    'use strict';

    angular
        .module('admin.sponsors.add', []) 
        .config(config);

    config.$inject = ['$stateProvider'];
    function config($stateProvider) {
        
        $stateProvider
            .state('admin.sponsors.add', {
                url: '/add',
                views: {
                    '@admin': {
                        templateUrl: '../views/admin/admin.sponsors.add.html',
                        controller: 'AdminSponsorsAddCtrl as vm'
                    }
                },
                data: {
                    is_granted: ['ROLE_ADMIN']
                }
            });
        
    }

})();
 
(function () {
    'use strict';

    angular
        .module('main.driver.account.uploadAvatar', [])
        .config(config);

    config.$inject = ['$stateProvider', '$urlRouterProvider', '$httpProvider'];
    function config($stateProvider, $urlRouterProvider, $httpProvider) {
        
        $stateProvider
            .state('main.driver.account.uploadAvatar', {
                url: '/upload-avatar',
                onEnter: function (ngDialog, $state) {
                    var options = {
                        templateUrl: '../views/driver/account/driver.account.uploadAvatar.html',
                        showClose: true,
                        controller: 'DriverAccountCtrl as vm',
                        appendClassName: 'ngdialog-default'
                    };

                    ngDialog 
                        .open(options)
                        .closePromise
                        .then(function (e) {
                            if (e.value)
                                $state.go('main.driver.account');
                            
                            return true;
                    });
                },
                data: {
                    is_granted: ['ROLE_DRIVER']
                }
            });
        
    }

})();
 
(function () {
    'use strict';
 
    angular
        .module('admin.drivers.profile.tabs', [
            'admin.drivers.profile.tabs.about',
            'admin.drivers.profile.tabs.location',
            'admin.drivers.profile.tabs.general',
            'admin.drivers.profile.tabs.links'
        ]) 
        .config(config);

    config.$inject = ['$stateProvider'];
    function config($stateProvider) {
        
        $stateProvider
            .state('admin.drivers.profile.tabs', {
                url: '',
                abstract: true,
                data: {
                    is_granted: ['ROLE_ADMIN']
                }
            });
        
    }

})();
 
(function () {
    'use strict';
 
    angular
        .module('admin.drivers.profile.tabs.about', []) 
        .config(config);

    config.$inject = ['$stateProvider'];
    function config($stateProvider) {
        
        $stateProvider
            .state('admin.drivers.profile.tabs.about', {
                url: '/about',
                views: {
                    'tabs@admin.drivers.profile': {
                        templateUrl: '../views/admin/profile/tabs/admin.drivers.profile.tabs.about.html'
                    }
                },
                data: {
                    is_granted: ['ROLE_ADMIN']
                }
            });
        
    }

})();
 
(function () {
    'use strict';
 
    angular
        .module('admin.drivers.profile.tabs.general', []) 
        .config(config);

    config.$inject = ['$stateProvider'];
    function config($stateProvider) {
        
        $stateProvider
            .state('admin.drivers.profile.tabs.general', {
                url: '',
                views: {
                    'tabs@admin.drivers.profile': {
                        templateUrl: '../views/admin/profile/tabs/admin.drivers.profile.tabs.general.html'
                    }
                },
                data: {
                    is_granted: ['ROLE_ADMIN']
                }
            });
        
    }

})();
 
(function () {
    'use strict';
 
    angular
        .module('admin.drivers.profile.tabs.links', [])
        .config(config);

    config.$inject = ['$stateProvider'];
    function config($stateProvider) {
        
        $stateProvider
            .state('admin.drivers.profile.tabs.links', {
                url: '/links',
                views: {
                    'tabs@admin.drivers.profile': {
                        templateUrl: '../views/admin/profile/tabs/admin.drivers.profile.tabs.links.html'
                    }
                },
                data: {
                    is_granted: ['ROLE_ADMIN']
                }
            });
        
    }

})();
 
(function () {
    'use strict';
 
    angular
        .module('admin.drivers.profile.tabs.location', []) 
        .config(config);

    config.$inject = ['$stateProvider'];
    function config($stateProvider) {
        
        $stateProvider
            .state('admin.drivers.profile.tabs.location', {
                url: '/location',
                views: {
                    'tabs@admin.drivers.profile': {
                        templateUrl: '../views/admin/profile/tabs/admin.drivers.profile.tabs.location.html'
                    }
                },
                data: {
                    is_granted: ['ROLE_ADMIN']
                }
            });
        
    }
    
})();
 