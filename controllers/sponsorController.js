/*
 FileName : sponsorController.js
 Date : 20th July 2017
 Description : This file consist of driver controller functions
 */

/* DEPENDENCIES */
var mongoose = require('mongoose');
var sponsorModel = require('./../models/sponsorModel');
var config = require('./../config/config.constants');
var async = require('async');

/* create driver account */
exports.createSponsor = function (req, res) {
  var sponsorObj = {
    sponsorName: req.body.spons_name,
    companyName: req.body.company_name,
    sponsorNumber: req.body.spons_count,
    sponsorAmmount: req.body.spons_ammount,
    partnerCompanyId: mongoose.Types.ObjectId(req.body.spons_partner),
    createdOn: new Date()
  };

  var sponsor = new sponsorModel(sponsorObj);

  sponsor.save(function (err, resultSponsor) {
    if (err) {
      return res.status(400).json(err);
    }
    return res.status(200).json({ message: 'Sponsor Created' });
  });
};

exports.getSponsors = function(req,res){
  if (!req.query.page || !req.query.limit) {
    return res.status(400).json({ message: 'Invalid parameters' });
  }
  var filteredQuery = {};
  filteredQuery = JSON.parse(JSON.stringify(filteredQuery));
  var resultObject = {
    sponsorData: [],
    totalRecords: 0
  };
  var offset = (parseInt(req.query.page) - 1) * parseInt(req.query.limit);
  async.parallel([
    function (callback) {
      sponsorModel.count(filteredQuery).exec(function (err, resultTotalCount) {
        if (err) {
          callback(err);
        }
        resultObject.totalRecords = resultTotalCount;
        callback(null, 'one');
      });
    },
    function (callback) {
      sponsorModel.find(filteredQuery).populate('partnerCompanyId').skip(offset).limit(parseInt(req.query.limit)).exec(function (err, resultSponsorList) {
        if (err) {
          callback(err);
        }
        
        resultObject.sponsorData = resultSponsorList;
        callback(null, 'two');
      });
    }
  ], function (err, result) {
    if (err) {
      return res.status(400).json(err);
    }
    return res.status(200).json({ data: resultObject });
  });
};
